package com.proyectactionsenl.output;

import com.proyectactionsenl.objects.Actividad;

public class OutputConsultaActividadForDocuments {
    
    private String result;
    private String resultDescripcion;
    private String version;
    private String Total_actividades;
    private Actividad[] Actividades;
    
    public OutputConsultaActividadForDocuments() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setTotal_actividades(String Total_actividades) {
        this.Total_actividades = Total_actividades;
    }

    public String getTotal_actividades() {
        return Total_actividades;
    }

    public void setActividades(Actividad[] Actividades) {
        this.Actividades = Actividades;
    }

    public Actividad[] getActividades() {
        return Actividades;
    }
}
