package com.proyectactionsenl.output;

import com.proyectactionsenl.objects.Proyectos;

public class OutputConsultaSitios {
    
    private String result;
    private String resultDescripcion;
    private String Version;
    private Proyectos[] Proyectos;
    
    public OutputConsultaSitios() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getVersion() {
        return Version;
    }

    public void setProyectos(Proyectos[] Proyectos) {
        this.Proyectos = Proyectos;
    }

    public Proyectos[] getProyectos() {
        return Proyectos;
    }
}
