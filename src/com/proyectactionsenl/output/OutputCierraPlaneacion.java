package com.proyectactionsenl.output;

import com.proyectactionsenl.objects.Proyectos;

public class OutputCierraPlaneacion {
    
    private String result;
    private String resultDescripcion;
    private String version;
    private Proyectos Proyecto;
    
    public OutputCierraPlaneacion() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setProyecto(Proyectos Proyecto) {
        this.Proyecto = Proyecto;
    }

    public Proyectos getProyecto() {
        return Proyecto;
    }
}
