package com.proyectactionsenl.logica;

import com.proyectactionsenl.objects.Actividad;
import com.proyectactionsenl.objects.Constantes;

import com.proyectactionsenl.objects.Plan;
import com.proyectactionsenl.objects.Proyectos;
import com.proyectactionsenl.objects.Puntas;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RecalculaGantt {
    public RecalculaGantt() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Proyectos recalculaGantt(String idcuenta,String id_bridge){
        Proyectos proyecto = null;
        try{
            
            QueryResult estructuraSF = Constantes.CRUD_SF.query_sf(Constantes.Q_SF_GET_STRUCTURA.replaceAll("idac", idcuenta));
            String matriz_actividatdes[][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_GET_STRUCT_FFM.replaceAll("idac", idcuenta));
            
            Map<String,String> map_cs = new HashMap<String,String>();
            Map<String,String> map_csp = new HashMap<String,String>();
            
                        
            for(SObject est : estructuraSF.getRecords()){
                String idcs = "";
                if(Constantes.UTILS.validateDataSF(est, "Cot_Sitio__r")){
                    SObject cs = (SObject) est.getField("Cot_Sitio__r");
                    map_cs.put((String) cs.getField("Id"), (String) cs.getField("Id"));
                    idcs = (String) cs.getField("Id");
                }
                
                map_csp.put((String) est.getField("Id"), idcs);
            
            }
            
            Map<String,Actividad> map_actividades = this.cargaActividades(matriz_actividatdes);
            Map<String,Plan> map_planes = this.cargaPlanes(map_csp, map_actividades, id_bridge);
            Map<String,Puntas> map_puntas = this.cargaPuntas(map_cs, map_planes,id_bridge);
            proyecto = this.cargaProyecto(map_puntas);
            
        }catch(Exception ex){
            System.err.println("Error : " + ex.toString());
        }
        
        return proyecto;
            
    }
    
    private Proyectos cargaProyecto(Map<String,Puntas> map_puntas){
        Proyectos proyecto = new Proyectos();
        
        try{
            ArrayList<Date> listInicialPlDate = new ArrayList<Date>();
            ArrayList<Date> listFinalPlDate = new ArrayList<Date>();
            ArrayList<Date> listInicialDate = new ArrayList<Date>();
            ArrayList<Date> listFinalDate = new ArrayList<Date>();
            ArrayList<Puntas> listaPuntas = new ArrayList<Puntas>();
            String fecha_actual = "";
            Float porcentaje = 0.0f;
            String avance = "0.0";
            String statusPlaneacion = "";
            for(Map.Entry<String, Puntas> pl : map_puntas.entrySet()){
                Puntas punta = pl.getValue();
                if(punta.getFechaFinReal() != null && !punta.getFechaFinReal().equals("")){
                    listInicialPlDate.add(Constantes.UTILS.StringToDateMM(punta.getFechaInicioPlaneada()));
                    listFinalPlDate.add(Constantes.UTILS.StringToDateMM(punta.getFechaFinPlaneada()));  
                    listInicialDate.add(Constantes.UTILS.StringToDateMM(punta.getFechaInicioReal()));
                    listFinalDate.add(Constantes.UTILS.StringToDateMM(punta.getFechaFinReal()));     
                }
                fecha_actual = punta.getFechaActual();
                porcentaje+=Float.valueOf(punta.getPorcentajeAvance());
                statusPlaneacion = punta.getEstatusPlaneacion();
                List<Plan> arrplan = new LinkedList<Plan>(Arrays.asList(punta.getPlanes()));
                ArrayList<Plan> arr = new ArrayList<>(); 
                
                for(int fila = 0; fila < arrplan.size(); fila++){
                        if(arrplan.get(fila).getActividades()==null || arrplan.get(fila).getActividades().length==0){
                            arr.add(arrplan.get(fila));
                        }
                }
                
                arrplan.removeAll(arr);
                
                punta.setPlanes(arrplan.toArray(new Plan[arrplan.size()]));
                
                if(punta.getPlanes()!= null && punta.getPlanes().length>0){
                    listaPuntas.add(punta);          
                }
            }  
                        
            if(porcentaje != 0 && listaPuntas.size() != 0){
                avance = String.valueOf(porcentaje/listaPuntas.size());
            }
                
            proyecto.setFechaInicioPlaneada(Constantes.UTILS.minDate(listInicialPlDate));
            proyecto.setFechaFinPlaneada(Constantes.UTILS.maxDate(listFinalPlDate));
            proyecto.setFechaInicioReal(Constantes.UTILS.minDate(listInicialDate));
            proyecto.setFechaFinReal(Constantes.UTILS.maxDate(listFinalDate));
            proyecto.setPorcentajeAvance(!proyecto.getFechaFinReal().equals("") ? Constantes.UTILS.valorDosDecimales(String.valueOf(porcentaje/map_puntas.size())):"");
            proyecto.setFechaActual(fecha_actual);
            proyecto.setPorcentajeEsperado(Constantes.UTILS.getPorcentajeEsperado(proyecto.getFechaInicioReal(), proyecto.getFechaFinReal(), fecha_actual));
            proyecto.setPuntas(listaPuntas.toArray(new Puntas[listaPuntas.size()]));           
            proyecto.setSemaforo(!proyecto.getFechaFinReal().equals("") ? Constantes.UTILS.getSemaforoEnPlaneacion(proyecto.getPorcentajeEsperado(),proyecto.getPorcentajeAvance(),statusPlaneacion,Constantes.UTILS.getEnTiempo(proyecto.getFechaFinPlaneada(), proyecto.getFechaFinReal())) : "");
            
        }catch(Exception ex){
            System.err.println("Error al cargar planes : " + ex.toString());
        }   
        
        return proyecto;
    }
    
    
    private Map<String,Puntas> cargaPuntas(Map<String,String> map_cs,Map<String,Plan> map_planes,String bridgeSf){
        Map<String,Puntas> map_puntas = new HashMap<String,Puntas>();
        
        try{
            
            for(Map.Entry<String, String> cs : map_cs.entrySet()){
                Puntas punta = new Puntas();
                punta.setId_cot_sitio(cs.getKey());
                ArrayList<Date> listInicialPlDate = new ArrayList<Date>();
                ArrayList<Date> listFinalPlDate = new ArrayList<Date>();
                ArrayList<Date> listInicialDate = new ArrayList<Date>();
                ArrayList<Date> listFinalDate = new ArrayList<Date>();
                ArrayList<Plan> listaPlanes = new ArrayList<Plan>();
                String fecha_actual = "";
                Float porcentaje = 0.0f;
                String avance = "0.0";
                String planeacionCerrada = "";
                for(Map.Entry<String, Plan> pl : map_planes.entrySet()){
                    Plan pal = pl.getValue();
                    if(pal.getId_cs().equals(cs.getKey())){
                        if(pal.getFechaFinReal() != null && !pal.getFechaFinReal().equals("")){
                            listInicialPlDate.add(Constantes.UTILS.StringToDateMM(pal.getFechaInicioPlaneada()));
                            listFinalPlDate.add(Constantes.UTILS.StringToDateMM(pal.getFechaFinPlaneada()));  
                            listInicialDate.add(Constantes.UTILS.StringToDateMM(pal.getFechaInicioReal()));
                            listFinalDate.add(Constantes.UTILS.StringToDateMM(pal.getFechaFinReal())); 
                        }
                        listaPlanes.add(pal);
                        fecha_actual = pal.getFechaActual();
                        porcentaje+= Float.valueOf(pal.getPorcentajeAvance());
                        planeacionCerrada = pal.getEstatusPlaneacion();
                    }
                }
                
                if(porcentaje != 0 && listaPlanes.size() != 0){
                    avance = String.valueOf(porcentaje/listaPlanes.size());
                }
                
                punta.setFechaInicioPlaneada(Constantes.UTILS.minDate(listInicialPlDate));
                punta.setFechaFinPlaneada(Constantes.UTILS.maxDate(listFinalPlDate));
                punta.setFechaInicioReal(Constantes.UTILS.minDate(listInicialDate));
                punta.setFechaFinReal(Constantes.UTILS.maxDate(listFinalDate));
                punta.setPorcentajeAvance(Constantes.UTILS.valorDosDecimales(String.valueOf(porcentaje/listaPlanes.size())));
                punta.setFechaActual(fecha_actual);
                punta.setPorcentajeEsperado(!punta.getFechaFinReal().equals("") ? Constantes.UTILS.getPorcentajeEsperado(punta.getFechaInicioReal(), punta.getFechaFinReal(), fecha_actual) : "");
                punta.setSemaforo(!punta.getFechaFinReal().equals("") ? Constantes.UTILS.getSemaforoEnPlaneacion(punta.getPorcentajeEsperado(),punta.getPorcentajeAvance(),planeacionCerrada,Constantes.UTILS.getEnTiempo(punta.getFechaFinPlaneada(), punta.getFechaFinReal())) :"");
                punta.setEstatusPlaneacion(planeacionCerrada);
                punta.setEnTiempo(Constantes.UTILS.getEnTiempo(punta.getFechaFinPlaneada(), punta.getFechaFinReal()));
                punta.setPlanes(listaPlanes.toArray(new Plan[listaPlanes.size()]));
                
                //if(listaPlanes.size()>0){
                    map_puntas.put(punta.getId_cot_sitio(), punta);        
               // }                                
                
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar planes : " + ex.toString());
        }   
        
        return map_puntas;
    }
    
    private Map<String,Plan> cargaPlanes(Map<String,String> map_csp,Map<String,Actividad> map_actividades,String bridgeSf){
        
        Map<String,Plan> map_planes = new HashMap<String,Plan>();
        
        try{
            for(Map.Entry<String, String> csps : map_csp.entrySet()){
                Plan plan = new Plan();
                plan.setId_csp(csps.getKey());
                ArrayList<Date> listInicialPlDate = new ArrayList<Date>();
                ArrayList<Date> listFinalPlDate = new ArrayList<Date>();
                ArrayList<Date> listInicialDate = new ArrayList<Date>();
                ArrayList<Date> listFinalDate = new ArrayList<Date>();
                ArrayList<Actividad> listActividades = new ArrayList<Actividad>();
                Float porcentaje = 0.0f;
                String avance = "0.0";
                String fecha_actual = "";
                ArrayList<String> arr_semaforo = new ArrayList<String>();
                String planeacionCerrada = "";
                String implementacionCerrada = "";
                String estatusPlaneacion = "";
                for(Map.Entry<String, Actividad> act : map_actividades.entrySet()){
                    Actividad acti = act.getValue();
                    if(acti.getId_csp().equals(csps.getKey())){
                        if(acti.getFecha_fin_real() != null && !acti.getFecha_fin_real().equals("")){
                            listInicialPlDate.add(Constantes.UTILS.StringToDateMM(acti.getFecha_inicio_planeada()));
                            listFinalPlDate.add(Constantes.UTILS.StringToDateMM(acti.getFecha_fin_planeada()));  
                            listInicialDate.add(Constantes.UTILS.StringToDateMM(acti.getFecha_inicio_real()));
                            listFinalDate.add(Constantes.UTILS.StringToDateMM(acti.getFecha_fin_real()));      
                        }
                        
                        if(acti.getId_csp().equals(bridgeSf)){
                            arr_semaforo.add(acti.getSemaforo());
                            listActividades.add(acti);                           
                        }
                        
                        planeacionCerrada = acti.getPlaneacionCerrada();
                        implementacionCerrada = acti.getPlanTermindo();
                        estatusPlaneacion = acti.getEstatusPlaneacion();
                            
                        porcentaje+=Float.valueOf(acti.getPorcentaje());
                    }
                    fecha_actual = acti.getHoy();
                }  
                
                if(porcentaje != 0 && listActividades.size() != 0){
                    avance = String.valueOf(porcentaje/listActividades.size());
                }
                Map<String,String> cont_planeacion = Constantes.UTILS.getContadoresPlaneacion(arr_semaforo);
                plan.setNum_fuera_tiempo(cont_planeacion.get(Constantes.C_FUERATIEMPO));
                plan.setNum_en_riesgo(cont_planeacion.get(Constantes.C_ENRIESGO));
                plan.setNum_en_tiempo(cont_planeacion.get(Constantes.C_ENTIEMPO));
                plan.setNum_pendientes(cont_planeacion.get(Constantes.C_PENDIENTES));
                plan.setFechaInicioPlaneada(Constantes.UTILS.minDate(listInicialPlDate));
                plan.setFechaFinPlaneada(Constantes.UTILS.maxDate(listFinalPlDate));
                plan.setFechaInicioReal(Constantes.UTILS.minDate(listInicialDate));
                plan.setFechaFinReal(Constantes.UTILS.maxDate(listFinalDate));
                plan.setPorcentajeAvance(Constantes.UTILS.valorDosDecimales(avance));
                plan.setFechaActual(fecha_actual);
                plan.setPorcentajeEsperado(!plan.getFechaFinReal().equals("") ? Constantes.UTILS.getPorcentajeEsperado(plan.getFechaInicioReal(), plan.getFechaFinReal(), fecha_actual) : "");
                plan.setSemaforo(!plan.getFechaFinReal().equals("") ? Constantes.UTILS.getSemaforoEnPlaneacion(plan.getPorcentajeEsperado(),plan.getPorcentajeAvance(),estatusPlaneacion,Constantes.UTILS.getEnTiempo(plan.getFechaFinPlaneada(), plan.getFechaFinReal())) : "");
                plan.setPlaneacionCerrada(planeacionCerrada);
                plan.setEstatusPlaneacion(estatusPlaneacion);
                plan.setEnTiempo(Constantes.UTILS.getEnTiempo(plan.getFechaFinPlaneada(), plan.getFechaFinReal()));
                plan.setImplementacionterminada(implementacionCerrada.equals("4") || implementacionCerrada.equals("5") ? "true" : "false");
                plan.setId_cs(csps.getValue());
                
                plan.setActividades(listActividades.toArray(new Actividad[listActividades.size()]));
                
               // if(listActividades.size()>0){
                    map_planes.put(plan.getId_csp(), plan);    
               // }
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar planes : " + ex.toString());
        }   
        
        return map_planes;
    }
    
    
    
    private Map<String,Actividad> cargaActividades(String matriz_actividatdes[][]){
        
        Map<String,Actividad> map_actividades =  new HashMap<String,Actividad>();
        
        try{
            
            for(int fila= 0; fila < matriz_actividatdes.length; fila++){
                Actividad actividad = new Actividad();
                actividad.setId_actividad(matriz_actividatdes[fila][0]);
                actividad.setNombre_actividad(matriz_actividatdes[fila][1]);
                actividad.setNombre_responsable(matriz_actividatdes[fila][2] != null ? matriz_actividatdes[fila][2] : "");
                actividad.setFecha_inicio_planeada(matriz_actividatdes[fila][3] != null ? matriz_actividatdes[fila][3] : "");
                actividad.setFecha_fin_planeada(matriz_actividatdes[fila][4] != null ? matriz_actividatdes[fila][4] : "");
                actividad.setFecha_inicio_real(matriz_actividatdes[fila][5] != null ? matriz_actividatdes[fila][5] : "");
                actividad.setFecha_fin_real(matriz_actividatdes[fila][6] != null ? matriz_actividatdes[fila][6] : "");
                actividad.setHoy(matriz_actividatdes[fila][7] != null ? matriz_actividatdes[fila][7] : "");                          
                actividad.setPorcentaje(matriz_actividatdes[fila][8] != null ? matriz_actividatdes[fila][8] : "0.0");
                actividad.setId_dependencia(matriz_actividatdes[fila][9] != null ? matriz_actividatdes[fila][9] :"");
                actividad.setTiene_Ot(matriz_actividatdes[fila][10]);
                actividad.setPorcentajeEsperado(!actividad.getFecha_fin_real().equals("") ? Constantes.UTILS.getPorcentajeEsperado(actividad.getFecha_inicio_real(),actividad.getFecha_fin_real(),actividad.getHoy()) : "");
                actividad.setSemaforo(!actividad.getFecha_fin_real().equals("") ? Constantes.UTILS.getSemaforoEnPlaneacion(actividad.getPorcentajeEsperado(), actividad.getPorcentaje(),matriz_actividatdes[fila][22],matriz_actividatdes[fila][23]) : "");
                actividad.setId_csp(matriz_actividatdes[fila][12]);
                actividad.setSe_puede_eliminar(matriz_actividatdes[fila][13]);
                actividad.setId_responsable(matriz_actividatdes[fila][14]);
                actividad.setTipo_intervencion(matriz_actividatdes[fila][15]);
                actividad.setSubtipo_intervencion(matriz_actividatdes[fila][16]);
                actividad.setUnidad_negocio(matriz_actividatdes[fila][17]);
                actividad.setId_tipo_actividad(matriz_actividatdes[fila][18]);
                actividad.setPlaneacionCerrada(matriz_actividatdes[fila][19]);
                actividad.setPlanTermindo(matriz_actividatdes[fila][20]);
                actividad.setId_OT(matriz_actividatdes[fila][21]);
                actividad.setEstatusPlaneacion(matriz_actividatdes[fila][22]);
                actividad.setEnTiempo(matriz_actividatdes[fila][23]);
                map_actividades.put(matriz_actividatdes[fila][0], actividad);            
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar Plan : " + ex.toString());
        }
        
        return map_actividades;
            
    }
    
}
