package com.proyectactionsenl.logica;

import com.proyectactionsenl.objects.Constantes;

public class Complementos {
    public Complementos() {
        super();
    }
    
    public static void registraComentarios(String comentario,String idactividad,String idUsuario,String bridgeSfd){
        
        try{
            
            Constantes.CRUD_FFM.insertaDB(Constantes.QR_INSERTA_COMENTARIOS.replaceAll("comentarios", comentario).replaceAll("id_usuario", idUsuario).replaceAll("id_cs", bridgeSfd).replaceAll("id_actividad", idactividad));    
                if(bridgeSfd!= null && !bridgeSfd.equals("")){
                    Constantes.CRUD_FFM.actualizaDB(Constantes.Q_UPDATE_COMENTARIO_GENERAL.replaceAll("comentario", comentario).replaceAll("idBridgeSf", bridgeSfd));
                }
        }catch(Exception ex){
            System.err.println("Error al registrar comentarios : " + ex.toString());    
        }
        
    }
    
}
