package com.proyectactionsenl.objects;

public class Puntas {    
    
    //--- Detalle
    private String Id_csp;
    private String Name_csp;
    private String Nombre_Paquete;
    private String Nombre_cs;
    private String Plaza;
    private String Direccion_sitio;
    private String Nombre_responsable_sitio;
    private String Telefono_contacto;
    private String Id_cuenta_factura;
    private String id_cuenta;
    private String Cuenta_factura;
    private String Canal_venta;
    private String Latitud;
    private String Longitud;
    
    // -- General
    private String Rown;
    private String Id_cot_sitio;
    private String Nombre_cot_sitio;
    private String Nombre_sitio;
    private String Fecha_fin_planeacion;
    private String Fecha_inicio;
    private String Fecha_fin;
    private String Porcentaje_avance;
    private String Status;
    private String PorcentajeEsperado;
    private String Punta_planeada;
    
    private String fechaInicioPlaneada;
    private String fechaFinPlaneada;
    private String fechaInicioReal;
    private String fechaFinReal;
    private String fechaActual;
    private String porcentajeAvance;
    private String porcentajeEsperado;
    private String semaforo;
    private String estatusPlaneacion;
    private String enTiempo;
    private Plan Planes[];
    private Actividad Actividades[];

    public void setId_csp(String Id_csp) {
        this.Id_csp = Id_csp;
    }

    public String getId_csp() {
        return Id_csp;
    }

    public void setName_csp(String Name_csp) {
        this.Name_csp = Name_csp;
    }

    public String getName_csp() {
        return Name_csp;
    }

    public void setNombre_Paquete(String Nombre_Paquete) {
        this.Nombre_Paquete = Constantes.UTILS.toInitCap(Nombre_Paquete);
    }

    public String getNombre_Paquete() {
        return Nombre_Paquete;
    }

    public void setNombre_cs(String Nombre_cs) {
        this.Nombre_cs = Nombre_cs;
    }

    public String getNombre_cs() {
        return Nombre_cs;
    }

    public void setPlaza(String Plaza) {
        this.Plaza = Plaza;
    }

    public String getPlaza() {
        return Plaza;
    }

    public void setDireccion_sitio(String Direccion_sitio) {
        this.Direccion_sitio = Constantes.UTILS.toInitCap(Direccion_sitio);
    }

    public String getDireccion_sitio() {
        return Direccion_sitio;
    }

    public void setNombre_responsable_sitio(String Nombre_responsable_sitio) {
        this.Nombre_responsable_sitio = Constantes.UTILS.toInitCap(Nombre_responsable_sitio);
    }

    public String getNombre_responsable_sitio() {
        return Nombre_responsable_sitio;
    }

    public void setTelefono_contacto(String Telefono_contacto) {
        this.Telefono_contacto = Telefono_contacto;
    }

    public String getTelefono_contacto() {
        return Telefono_contacto;
    }

    public void setId_cuenta_factura(String Id_cuenta_factura) {
        this.Id_cuenta_factura = Id_cuenta_factura;
    }

    public String getId_cuenta_factura() {
        return Id_cuenta_factura;
    }

    public void setId_cuenta(String id_cuenta) {
        this.id_cuenta = id_cuenta;
    }

    public String getId_cuenta() {
        return id_cuenta;
    }

    public void setCuenta_factura(String Cuenta_factura) {
        this.Cuenta_factura = Cuenta_factura;
    }

    public String getCuenta_factura() {
        return Cuenta_factura;
    }

    public void setCanal_venta(String Canal_venta) {
        this.Canal_venta = Canal_venta;
    }

    public String getCanal_venta() {
        return Canal_venta;
    }

    public void setLatitud(String Latitud) {
        this.Latitud = Latitud;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLongitud(String Longitud) {
        this.Longitud = Longitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setRown(String Rown) {
        this.Rown = Rown;
    }

    public String getRown() {
        return Rown;
    }

    public void setId_cot_sitio(String Id_cot_sitio) {
        this.Id_cot_sitio = Id_cot_sitio;
    }

    public String getId_cot_sitio() {
        return Id_cot_sitio;
    }

    public void setNombre_cot_sitio(String Nombre_cot_sitio) {
        this.Nombre_cot_sitio = Nombre_cot_sitio;
    }

    public String getNombre_cot_sitio() {
        return Nombre_cot_sitio;
    }

    public void setNombre_sitio(String Nombre_sitio) {
        this.Nombre_sitio = Nombre_sitio;
    }

    public String getNombre_sitio() {
        return Nombre_sitio;
    }

    public void setFecha_fin_planeacion(String Fecha_fin_planeacion) {
        this.Fecha_fin_planeacion = Fecha_fin_planeacion;
    }

    public String getFecha_fin_planeacion() {
        return Fecha_fin_planeacion;
    }

    public void setFecha_inicio(String Fecha_inicio) {
        this.Fecha_inicio = Fecha_inicio;
    }

    public String getFecha_inicio() {
        return Fecha_inicio;
    }

    public void setFecha_fin(String Fecha_fin) {
        this.Fecha_fin = Fecha_fin;
    }

    public String getFecha_fin() {
        return Fecha_fin;
    }

    public void setPorcentaje_avance(String Porcentaje_avance) {
        this.Porcentaje_avance = Porcentaje_avance;
    }

    public String getPorcentaje_avance() {
        return Porcentaje_avance;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getStatus() {
        return Status;
    }

    public void setSemaforo(String Semaforo) {
        this.semaforo = Semaforo;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setPorcentajeEsperado(String PorcentajeEsperado) {
        this.PorcentajeEsperado = PorcentajeEsperado;
    }

    public String getPorcentajeEsperado() {
        return PorcentajeEsperado;
    }

    public void setPunta_planeada(String Punta_planeada) {
        this.Punta_planeada = Punta_planeada;
    }

    public String getPunta_planeada() {
        return Punta_planeada;
    }

    public void setActividades(Actividad[] Actividades) {
        this.Actividades = Actividades;
    }

    public Actividad[] getActividades() {
        return Actividades;
    }

    public void setFechaInicioPlaneada(String fechaInicioPlaneada) {
        this.fechaInicioPlaneada = fechaInicioPlaneada;
    }

    public String getFechaInicioPlaneada() {
        return fechaInicioPlaneada;
    }

    public void setFechaFinPlaneada(String fechaFinPlaneada) {
        this.fechaFinPlaneada = fechaFinPlaneada;
    }

    public String getFechaFinPlaneada() {
        return fechaFinPlaneada;
    }

    public void setFechaInicioReal(String fechaInicioReal) {
        this.fechaInicioReal = fechaInicioReal;
    }

    public String getFechaInicioReal() {
        return fechaInicioReal;
    }

    public void setFechaFinReal(String fechaFinReal) {
        this.fechaFinReal = fechaFinReal;
    }

    public String getFechaFinReal() {
        return fechaFinReal;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setPorcentajeAvance(String porcentajeAvance) {
        this.porcentajeAvance = porcentajeAvance;
    }

    public String getPorcentajeAvance() {
        return porcentajeAvance;
    }

    public void setPorcentajeEsperado1(String porcentajeEsperado) {
        this.porcentajeEsperado = porcentajeEsperado;
    }

    public String getPorcentajeEsperado1() {
        return porcentajeEsperado;
    }

    public void setPlanes(Plan[] Planes) {
        this.Planes = Planes;
    }

    public Plan[] getPlanes() {
        return Planes;
    }

    public void setEstatusPlaneacion(String estatusPlaneacion) {
        this.estatusPlaneacion = estatusPlaneacion;
    }

    public String getEstatusPlaneacion() {
        return estatusPlaneacion;
    }

    public void setEnTiempo(String enTiempo) {
        this.enTiempo = enTiempo;
    }

    public String getEnTiempo() {
        return enTiempo;
    }
}
