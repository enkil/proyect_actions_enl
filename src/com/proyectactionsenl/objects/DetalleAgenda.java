package com.proyectactionsenl.objects;

public class DetalleAgenda {
    private String Fecha;
    private String Turno;
    private String Id_ot;
    private String Orden_Servicio;
    
    public DetalleAgenda() {
        super();
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setTurno(String Turno) {
        this.Turno = Constantes.UTILS.toInitCap(Turno);
    }

    public String getTurno() {
        return Turno;
    }

    public void setOrden_Servicio(String Orden_Servicio) {
        this.Orden_Servicio = Orden_Servicio;
    }

    public String getOrden_Servicio() {
        return Orden_Servicio;
    }

    public void setId_ot(String Id_ot) {
        this.Id_ot = Id_ot;
    }

    public String getId_ot() {
        return Id_ot;
    }
}
