package com.proyectactionsenl.objects;

import weblogic.xml.security.specs.User;

public class Login {
    
    private String Us;
    private String Pa;
    private String Ip;
    
    public Login() {
        super();
    }

    public void setUs(String Us) {
        this.Us = Us;
    }

    public String getUs() {
        return Us;
    }

    public void setPa(String Pa) {
        this.Pa = Pa;
    }

    public String getPa() {
        return Pa;
    }

    public void setIp(String Ip) {
        this.Ip = Ip;
    }

    public String getIp() {
        return Ip;
    }
}
