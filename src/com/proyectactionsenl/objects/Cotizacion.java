package com.proyectactionsenl.objects;


public class Cotizacion {
    
    private String Id_cotizacion;
    private String Folio_cotizacion;
    private Sitio[] Sitios;
    
    public Cotizacion() {
        super();
    }

    public void setId_cotizacion(String Id_cotizacion) {
        this.Id_cotizacion = Id_cotizacion;
    }

    public String getId_cotizacion() {
        return Id_cotizacion;
    }

    public void setFolio_cotizacion(String Folio_cotizacion) {
        this.Folio_cotizacion = Folio_cotizacion;
    }

    public String getFolio_cotizacion() {
        return Folio_cotizacion;
    }

    public void setSitios(Sitio[] Sitios) {
        this.Sitios = Sitios;
    }

    public Sitio[] getSitios() {
        return Sitios;
    }
}
