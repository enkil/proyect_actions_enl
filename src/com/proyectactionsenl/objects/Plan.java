package com.proyectactionsenl.objects;

public class Plan {
    public Plan() {
        super();
    }
    
    private String Id_cs;
    private String Id_csp;
    private String Num_fuera_tiempo;
    private String Num_en_riesgo;
    private String Num_en_tiempo;
    private String Num_pendientes;
    private String fechaInicioPlaneada;
    private String fechaFinPlaneada;
    private String fechaInicioReal;
    private String fechaFinReal;
    private String fechaActual;
    private String porcentajeAvance;
    private String porcentajeEsperado;
    private String semaforo;
    private String PlaneacionCerrada;
    private String implementacionterminada;
    private String estatusPlaneacion;
    private String enTiempo;
    private Actividad actividades[];


    public void setNum_fuera_tiempo(String Num_fuera_tiempo) {
        this.Num_fuera_tiempo = Num_fuera_tiempo;
    }

    public String getNum_fuera_tiempo() {
        return Num_fuera_tiempo;
    }

    public void setNum_en_riesgo(String Num_en_riesgo) {
        this.Num_en_riesgo = Num_en_riesgo;
    }

    public String getNum_en_riesgo() {
        return Num_en_riesgo;
    }

    public void setNum_en_tiempo(String Num_en_tiempo) {
        this.Num_en_tiempo = Num_en_tiempo;
    }

    public String getNum_en_tiempo() {
        return Num_en_tiempo;
    }

    public void setNum_pendientes(String Num_pendientes) {
        this.Num_pendientes = Num_pendientes;
    }

    public String getNum_pendientes() {
        return Num_pendientes;
    }

    public void setId_csp(String Id_csp) {
        this.Id_csp = Id_csp;
    }

    public String getId_csp() {
        return Id_csp;
    }

    public void setFechaInicioPlaneada(String fechaInicioPlaneada) {
        this.fechaInicioPlaneada = fechaInicioPlaneada;
    }

    public String getFechaInicioPlaneada() {
        return fechaInicioPlaneada;
    }

    public void setFechaFinPlaneada(String fechaFinPlaneada) {
        this.fechaFinPlaneada = fechaFinPlaneada;
    }

    public String getFechaFinPlaneada() {
        return fechaFinPlaneada;
    }

    public void setFechaInicioReal(String fechaInicioReal) {
        this.fechaInicioReal = fechaInicioReal;
    }

    public String getFechaInicioReal() {
        return fechaInicioReal;
    }

    public void setFechaFinReal(String fechaFinReal) {
        this.fechaFinReal = fechaFinReal;
    }

    public String getFechaFinReal() {
        return fechaFinReal;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setPorcentajeAvance(String porcentajeAvance) {
        this.porcentajeAvance = porcentajeAvance;
    }

    public String getPorcentajeAvance() {
        return porcentajeAvance;
    }

    public void setPorcentajeEsperado(String porcentajeEsperado) {
        this.porcentajeEsperado = porcentajeEsperado;
    }

    public String getPorcentajeEsperado() {
        return porcentajeEsperado;
    }

    public void setId_cs(String Id_cs) {
        this.Id_cs = Id_cs;
    }

    public String getId_cs() {
        return Id_cs;
    }

    public void setSemaforo(String semaforo) {
        this.semaforo = semaforo;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setActividades(Actividad[] actividades) {
        this.actividades = actividades;
    }

    public Actividad[] getActividades() {
        return actividades;
    }

    public void setPlaneacionCerrada(String PlaneacionCerrada) {
        this.PlaneacionCerrada = PlaneacionCerrada;
    }

    public String getPlaneacionCerrada() {
        return PlaneacionCerrada;
    }

    public void setImplementacionterminada(String implementacionterminada) {
        this.implementacionterminada = implementacionterminada;
    }

    public String getImplementacionterminada() {
        return implementacionterminada;
    }

    public void setEstatusPlaneacion(String estatusPlaneacion) {
        this.estatusPlaneacion = estatusPlaneacion;
    }

    public String getEstatusPlaneacion() {
        return estatusPlaneacion;
    }

    public void setEnTiempo(String enTiempo) {
        this.enTiempo = enTiempo;
    }

    public String getEnTiempo() {
        return enTiempo;
    }
}
