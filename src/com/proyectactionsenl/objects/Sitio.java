package com.proyectactionsenl.objects;


public class Sitio {
    
    private String Id_sitio;
    private String Id_cs;
    private String nombre_cot_sitio;
    private String Id_csp;
    private String Name_csp;
    private String Paquete;
    private String Plaza;
    private String Direccion_sitio;
    private String Nombre_responsable_sitio;
    private String Telefono_contacto;
    private String Canal_venta;
    private String Latitud;
    private String Longitud;
    private String Unidad_negocio;
    private String Tipo_intervencion;
    private String Subtipo_intervencion;
    private String Id_cuenta_factura;
    private String Num_cuenta_factura;
    private String id_cuenta;
    private String Region;
    private String Ciudad;
    private String Distrito;
    private String Cluster;
    private String Calle;
    private String Colonia;
    private String Municipio;
    private String Estado;
    private String Cp;
    private String Num_interior;
    private String Num_exterior;
    
    public Sitio() {
        super();
    }

    public void setId_cs(String Id_cs) {
        this.Id_cs = Id_cs;
    }

    public String getId_cs() {
        return Id_cs;
    }

    public void setNombre_cot_sitio(String nombre_cot_sitio) {
        this.nombre_cot_sitio = nombre_cot_sitio;
    }

    public String getNombre_cot_sitio() {
        return nombre_cot_sitio;
    }

    public void setId_csp(String Id_csp) {
        this.Id_csp = Id_csp;
    }

    public String getId_csp() {
        return Id_csp;
    }

    public void setName_csp(String Name_csp) {
        this.Name_csp = Name_csp;
    }

    public String getName_csp() {
        return Name_csp;
    }

    public void setPaquete(String Paquete) {
        this.Paquete = Constantes.UTILS.toInitCap(Paquete);
    }

    public String getPaquete() {
        return Paquete;
    }

    public void setPlaza(String Plaza) {
        this.Plaza = Plaza;
    }

    public String getPlaza() {
        return Plaza;
    }

    public void setDireccion_sitio(String Direccion_sitio) {
        this.Direccion_sitio = Constantes.UTILS.toInitCap(Direccion_sitio);
    }

    public String getDireccion_sitio() {
        return Direccion_sitio;
    }

    public void setNombre_responsable_sitio(String Nombre_responsable_sitio) {
        this.Nombre_responsable_sitio = Constantes.UTILS.toInitCap(Nombre_responsable_sitio);
    }

    public String getNombre_responsable_sitio() {
        return Nombre_responsable_sitio;
    }

    public void setTelefono_contacto(String Telefono_contacto) {
        this.Telefono_contacto = Telefono_contacto;
    }

    public String getTelefono_contacto() {
        return Telefono_contacto;
    }

    public void setCanal_venta(String Canal_venta) {
        this.Canal_venta = Canal_venta;
    }

    public String getCanal_venta() {
        return Canal_venta;
    }

    public void setLatitud(String Latitud) {
        this.Latitud = Latitud;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLongitud(String Longitud) {
        this.Longitud = Longitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setUnidad_negocio(String Unidad_negocio) {
        this.Unidad_negocio = Unidad_negocio;
    }

    public String getUnidad_negocio() {
        return Unidad_negocio;
    }

    public void setTipo_intervencion(String Tipo_intervencion) {
        this.Tipo_intervencion = Tipo_intervencion;
    }

    public String getTipo_intervencion() {
        return Tipo_intervencion;
    }

    public void setSubtipo_intervencion(String Subtipo_intervencion) {
        this.Subtipo_intervencion = Subtipo_intervencion;
    }

    public String getSubtipo_intervencion() {
        return Subtipo_intervencion;
    }

    public void setId_cuenta_factura(String Id_cuenta_factura) {
        this.Id_cuenta_factura = Id_cuenta_factura;
    }

    public String getId_cuenta_factura() {
        return Id_cuenta_factura;
    }

    public void setNum_cuenta_factura(String Num_cuenta_factura) {
        this.Num_cuenta_factura = Num_cuenta_factura;
    }

    public String getNum_cuenta_factura() {
        return Num_cuenta_factura;
    }

    public void setId_cuenta(String id_cuenta) {
        this.id_cuenta = id_cuenta;
    }

    public String getId_cuenta() {
        return id_cuenta;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getRegion() {
        return Region;
    }

    public void setCiudad(String Ciudad) {
        this.Ciudad = Ciudad;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setDistrito(String Distrito) {
        this.Distrito = Distrito;
    }

    public String getDistrito() {
        return Distrito;
    }

    public void setCluster(String Cluster) {
        this.Cluster = Cluster;
    }

    public String getCluster() {
        return Cluster;
    }

    public void setColonia(String Colonia) {
        this.Colonia = Constantes.UTILS.toInitCap(Colonia);
    }

    public String getColonia() {
        return Colonia;
    }

    public void setMunicipio(String Municipio) {
        this.Municipio = Constantes.UTILS.toInitCap(Municipio);
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setEstado(String Estado) {
        this.Estado = Constantes.UTILS.toInitCap(Estado);
    }

    public String getEstado() {
        return Estado;
    }

    public void setCp(String Cp) {
        this.Cp = Cp;
    }

    public String getCp() {
        return Cp;
    }

    public void setNum_interior(String Num_interior) {
        this.Num_interior = Num_interior;
    }

    public String getNum_interior() {
        return Num_interior;
    }

    public void setNum_exterior(String Num_exterior) {
        this.Num_exterior = Num_exterior;
    }

    public String getNum_exterior() {
        return Num_exterior;
    }

    public void setCalle(String Calle) {
        this.Calle = Constantes.UTILS.toInitCap(Calle);
    }

    public String getCalle() {
        return Calle;
    }

    public void setId_sitio(String Id_sitio) {
        this.Id_sitio = Id_sitio;
    }

    public String getId_sitio() {
        return Id_sitio;
    }
}
