package com.proyectactionsenl.objects;


public class Proyectos {
    //-- variables para find 
    private String Nombre_cliente;
    private Cotizacion Cotizaciones;
    
    
    //-- variables para busqueda primer nivel
    private String Id_cotizacion;
    private String Folio_cotizacion;
    //-- datos en planeacion 
    private String Fecha_inicio; 
    private String Fecha_fin;
    private String Fecha_fin_Planeada;
    private String Avance;
    //-- En mesa de control
    private String Nombre_Contacto;
    private String Telefono_contacto;
    private String Fecha_creacion;
    //-- Datos en pendientes planeacion
    private String Sector;
    private String Segmento;
    
    private String Numero_sitios;
    private String Tiempo_mesa;
    private String PorcentajeEsperado;
    private String[] Cuentas_Factura;
    private String Fecha_cierre;
    private String Plazo;
    private String Numero_oportunidad;
    private String Es_Top_5000;
    private String fechaInicioPlaneada;
    private String fechaFinPlaneada;
    private String fechaInicioReal;
    private String fechaFinReal;
    private String fechaActual;
    private String porcentajeAvance;
    private String porcentajeEsperado;
    private String semaforo;
    private Puntas Puntas[];
    
    
    public Proyectos() {
        super();
    }

    public void setNombre_cliente(String Nombre_cliente) {
        this.Nombre_cliente = Constantes.UTILS.toInitCap(Nombre_cliente);
    }

    public String getNombre_cliente() {
        return Nombre_cliente;
    }

    public void setCotizaciones(Cotizacion Cotizaciones) {
        this.Cotizaciones = Cotizaciones;
    }

    public Cotizacion getCotizaciones() {
        return Cotizaciones;
    }

    public void setId_cotizacion(String Id_cotizacion) {
        this.Id_cotizacion = Id_cotizacion;
    }

    public String getId_cotizacion() {
        return Id_cotizacion;
    }

    public void setFolio_cotizacion(String Folio_cotizacion) {
        this.Folio_cotizacion = Folio_cotizacion;
    }

    public String getFolio_cotizacion() {
        return Folio_cotizacion;
    }

    public void setFecha_inicio(String Fecha_inicio) {
        this.Fecha_inicio = Fecha_inicio;
    }

    public String getFecha_inicio() {
        return Fecha_inicio;
    }

    public void setFecha_fin(String Fecha_fin) {
        this.Fecha_fin = Fecha_fin;
    }

    public String getFecha_fin() {
        return Fecha_fin;
    }

    public void setFecha_fin_Planeada(String Fecha_fin_Planeada) {
        this.Fecha_fin_Planeada = Fecha_fin_Planeada;
    }

    public String getFecha_fin_Planeada() {
        return Fecha_fin_Planeada;
    }

    public void setAvance(String Avance) {
        this.Avance = Avance;
    }

    public String getAvance() {
        return Avance;
    }

    public void setNombre_Contacto(String Nombre_Contacto) {
        this.Nombre_Contacto = Constantes.UTILS.toInitCap(Nombre_Contacto);
    }

    public String getNombre_Contacto() {
        return Nombre_Contacto;
    }

    public void setTelefono_contacto(String Telefono_contacto) {
        this.Telefono_contacto = Telefono_contacto;
    }

    public String getTelefono_contacto() {
        return Telefono_contacto;
    }

    public void setFecha_creacion(String Fecha_creacion) {
        this.Fecha_creacion = Fecha_creacion;
    }

    public String getFecha_creacion() {
        return Fecha_creacion;
    }

    public void setSegmento(String Segmento) {
        this.Segmento = Constantes.UTILS.toInitCap(Segmento);
    }

    public String getSegmento() {
        return Segmento;
    }

    public void setNumero_sitios(String Numero_sitios) {
        this.Numero_sitios = Numero_sitios;
    }

    public String getNumero_sitios() {
        return Numero_sitios;
    }

    public void setTiempo_mesa(String Tiempo_mesa) {
        this.Tiempo_mesa = Tiempo_mesa;
    }

    public String getTiempo_mesa() {
        return Tiempo_mesa;
    }

    public void setSemaforo(String Semaforo) {
        this.semaforo = Semaforo;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setPorcentajeEsperado(String PorcentajeEsperado) {
        this.PorcentajeEsperado = PorcentajeEsperado;
    }

    public String getPorcentajeEsperado() {
        return PorcentajeEsperado;
    }

    public void setCuentas_Factura(String[] Cuentas_Factura) {
        this.Cuentas_Factura = Cuentas_Factura;
    }

    public String[] getCuentas_Factura() {
        return Cuentas_Factura;
    }

    public void setSector(String Sector) {
        this.Sector = Sector;
    }

    public String getSector() {
        return Sector;
    }

    public void setFecha_cierre(String Fecha_cierre) {
        this.Fecha_cierre = Fecha_cierre;
    }

    public String getFecha_cierre() {
        return Fecha_cierre;
    }

    public void setPlazo(String Plazo) {
        this.Plazo = Plazo;
    }

    public String getPlazo() {
        return Plazo;
    }

    public void setNumero_oportunidad(String Numero_oportunidad) {
        this.Numero_oportunidad = Numero_oportunidad;
    }

    public String getNumero_oportunidad() {
        return Numero_oportunidad;
    }

    public void setEs_Top_5000(String Es_Top_5000) {
        this.Es_Top_5000 = Es_Top_5000;
    }

    public String getEs_Top_5000() {
        return Es_Top_5000;
    }

    public void setPuntas(Puntas[] Puntas) {
        this.Puntas = Puntas;
    }

    public Puntas[] getPuntas() {
        return Puntas;
    }

    public void setFechaInicioPlaneada(String fechaInicioPlaneada) {
        this.fechaInicioPlaneada = fechaInicioPlaneada;
    }

    public String getFechaInicioPlaneada() {
        return fechaInicioPlaneada;
    }

    public void setFechaFinPlaneada(String fechaFinPlaneada) {
        this.fechaFinPlaneada = fechaFinPlaneada;
    }

    public String getFechaFinPlaneada() {
        return fechaFinPlaneada;
    }

    public void setFechaInicioReal(String fechaInicioReal) {
        this.fechaInicioReal = fechaInicioReal;
    }

    public String getFechaInicioReal() {
        return fechaInicioReal;
    }

    public void setFechaFinReal(String fechaFinReal) {
        this.fechaFinReal = fechaFinReal;
    }

    public String getFechaFinReal() {
        return fechaFinReal;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setPorcentajeAvance(String porcentajeAvance) {
        this.porcentajeAvance = porcentajeAvance;
    }

    public String getPorcentajeAvance() {
        return porcentajeAvance;
    }

    public void setPorcentajeEsperado1(String porcentajeEsperado) {
        this.porcentajeEsperado = porcentajeEsperado;
    }

    public String getPorcentajeEsperado1() {
        return porcentajeEsperado;
    }


}
