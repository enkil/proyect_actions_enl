package com.proyectactionsenl.objects;

import com.proyectactionsenl.utilerias.ConexionDB;
import com.proyectactionsenl.utilerias.CrudFFM;
import com.proyectactionsenl.utilerias.CrudSF;
import com.proyectactionsenl.utilerias.Utilerias;

import java.util.Properties;

public class Constantes {
    //----- Instancias de clases
    public static final Utilerias UTILS = new Utilerias();
    public static final ConexionDB CONECTION_DB = new ConexionDB();
    public static final CrudSF CRUD_SF = new CrudSF();
    public static final CrudFFM CRUD_FFM = new CrudFFM();
    //----- contantes ws
    public static final String PATH_PROPERTIES = "/resources/Configuracion.properties";
    public static final Properties CONFIG_PROPERTIES = UTILS.getConfiguracion();
    public static final String GENERIC_CONSULT = "Select Name From Account LIMIT 1";
    
    //----- Constantes desde Properties file    
    public static final String RESULT_SUSSES = CONFIG_PROPERTIES.getProperty("ws.susses_id");
    public static final String RESULT_ERROR = CONFIG_PROPERTIES.getProperty("ws.error_id");
    public static final String RESULTSESCRIPTION_SUSSES = CONFIG_PROPERTIES.getProperty("ws.msj_susses");
    public static final String WS_VERSION = CONFIG_PROPERTIES.getProperty("ws.version");
    
    //----- Constantes de conexion a bd
    public static final String DB_TYPE_CONECTION = CONFIG_PROPERTIES.getProperty("servidor.type_conection");
    public static final String DB_HOST = CONFIG_PROPERTIES.getProperty("servidor.host");
    public static final String DB_PORT = CONFIG_PROPERTIES.getProperty("servidor.port");
    public static final String DB_DBNAME = CONFIG_PROPERTIES.getProperty("servidor.namedb");
    public static final String DB_US = CONFIG_PROPERTIES.getProperty("servidor.us");
    public static final String DB_PA = CONFIG_PROPERTIES.getProperty("servidor.pa");
    public static final String DB_JNDI = CONFIG_PROPERTIES.getProperty("servidor.jndi");
    
    public static final String QR_GET_ID_SESSION_SF = CONFIG_PROPERTIES.getProperty("query.get_id_session_sf");
    public static final String QR_CONFIG_IN_DB = CONFIG_PROPERTIES.getProperty("query.conf_in_db");
    public static final String QR_UPDATE_SI_SF = CONFIG_PROPERTIES.getProperty("query.update_sessionid_sf");
    
    //-- Consula proyectos asignados 
    public static final String QR_GET_PROYECTOS_ASIGNADOS = CONFIG_PROPERTIES.getProperty("query.get_proyectos_asignados");
    
    //-- Consulta Actividades para documentos 
    public static final String QR_GET_ACT_FOR_DOCUMENTS = CONFIG_PROPERTIES.getProperty("query.get_activities_for_documents");
    
    //-- Termina proyecto
    public static final String STATUS_IMPLEMENTACION = "Implementado";
    
    public static final String KEY_US_SF = "AUTENTICATION_SF_US";
    public static final String KEY_PA_SF = "AUTENTICATION_SF_PS";
    public static final String KEY_END_POINT_SF = "END_POINT_WS_SF";
    
    public static final String KEY_SEX_SF_ID = "ID_SEXO_CONTACT";
    
    public static final String KEY_SEGUNDOS_DOS_DIAS = "SEG_TWO_DAY";
    public static final String KEY_SEGUNDOS_MEDIODIA = "SEG_DAY_M";
    public static final String KEY_SEGUNDOS_UNDIA = "SEG_DAY";
    public static final String KEY_PORCENTAJE_OK = "PORCENTAJE_OK";
    public static final String KEY_PORCENTAJE_RIESGO = "PORCENTAJE_RIESGO";
    public static final String KEY_PORCENTAJE_FUERA = "PORCENTAJE_FUERATIEMPO";
    public static final String KEY_COLOR_FUERA_TIEMPO = "FUERATIEMPO";
    public static final String KEY_COLOR_RIESGO = "ENRIESGO";
    public static final String KEY_COLOR_ENTIEMPO = "ENTIEMPO";
    public static final String KEY_KPI_PE_DIAS = "KPI_PE_DIAS";
    public static final String KEY_KPI_PI_DIAS = "KPI_PI_DIAS";
    public static final String KEY_QR_PROYECTOS_MESA = "QR_PROYECTOS_MESA";
    public static final String KEY_QR_BANDEJA_EN_PLANEACION = "QR_PROYECTOS_EPL";
    public static final String KEY_QR_BANDEJA_FACTURANDO = "QR_PROYECTOS_FC";
    public static final String KEY_URL_DASHBOARD = "URL_DASHBOARD_RC";
    //--------------------------------------------------------------------------------------------------------
    
    // -- CODE CONSTANTS
    public final static String QR_CONFIGURACION_FFM = "SELECT ID,DESCRIPCION,VALOR FROM ENL_GENERAL_CONFIG WHERE DESCRIPCION IN ('AUTENTICATION_SF_US','AUTENTICATION_SF_PS','ID_SEXO_CONTACT','END_POINT_WS_SF','END_POINT_WS_CREAOS_SF','END_POINT_WS_CREAOT_FFM','BASIC_AUTEBTICATION_PS','BASIC_AUTENTICATION_US')"; 
    public final static String CONSTANT_PLAZAS = "plazas";
    public final static String CONSTANT_DISTRITOS = "distritos";
    public final static String CONSTANT_FECHA_IN = "fechain";
    public final static String CONSTANT_FECHA_FIN = "fechafin";
    
    public final static String QR_CONDICIONES_BANDEJAS ="SELECT VALOR CONDICIONES_BDSF FROM ENL_GENERAL_CONFIG WHERE DESCRIPCION = 'key_bandeja'";
    public final static String QR_BASE_BANDEJAS = "Select Id,OrdenServicio__r.Name,OrdenServicio__c,Cot_Sitio__r.Sitio__c,CuentaFactura__c,Cot_Sitio__r.Name,Name,Cotizacion__r.Name,Ticket__c,Ticket__r.CaseNumber,CuentaFacturaNumero__c,Direccion_Sitio__c,TSganada__c,Plaza__c,NombrePlan__c,Cot_Sitio__r.Sitio__r.Calle__c,Cot_Sitio__r.Sitio__r.NumeroExterior__c,Cot_Sitio__r.Sitio__r.NumeroInterior__c,Cot_Sitio__r.Sitio__r.Colonia__c,Cot_Sitio__r.Sitio__r.DelegacionMunicipio__c,Cot_Sitio__r.Sitio__r.Estado__c,Cot_Sitio__r.Sitio__r.Ciudad__c,Cot_Sitio__r.Sitio__r.CodigoPostal__c,Cot_Sitio__r.Sitio__r.EntreCalles__c,Cot_Sitio__r.Sitio__r.ReferenciaCalle__c,Cot_Sitio__r.Sitio__r.Cobertura__c,Cot_Sitio__r.Sitio__r.Distrito__c, Cot_Sitio__r.Sitio__r.Clouster__c,Cot_Sitio__r.Sitio__r.Region__c,Cot_Sitio__r.Sitio__r.Geolocalizacion__Latitude__s,Cot_Sitio__r.Sitio__r.Geolocalizacion__Longitude__s,CuentaFactura__r.IdCuenta__c,CuentaFactura__r.IdCuenta__r.RazonSocial__c,Cot_Sitio__r.Sitio__r.ContactoPrincipalSitio__r.Name,DP_Plan__r.TipoPlan__c,CuentaFactura__r.Celular__c,CuentaFactura__r.TelefonoPrincipal__c,CuentaFactura__r.CodigoPostalInstalacion__c,CuentaFactura__r.CodigoPostal__c,CuentaFactura__r.CodigoPostalFacturacion__c  From Cot_SitioPlan__c  ";
    
    // -- Nombres objetos sf
    public final static String COT_SITIO = "Cot_Sitio__r";
    public final static String SITIO = "Sitio__r";
    public final static String CONTACTO_PINCIPAL_SITIO = "ContactoPrincipalSitio__r";
    public final static String OS = "OrdenServicio__c";
    public final static String COTIZACION = "Cotizacion__r";
    public final static String TICKET = "Ticket__r";
    public final static String CUENTA_FACTURA = "CuentaFactura__r";
    public final static String CUENTA = "IdCuenta__r";
    public final static String DP_PLAN = "DP_Plan__r";
    public final static String STATUS_OS = "Agendado";
    public final static String STATUS_OS_CONF = "Confirmado";
    public final static String CUENTA_FACTURA_ORG = "CuentaFactura__c";
    public final static String OPPORTUNITY_TYPE = "Opportunity";
    public final static String CSP_TYPE = "Cot_SitioPlan__c";
    
    
    public static final String C_ENTIEMPO = "ET";
    public static final String C_ENRIESGO = "ER";
    public static final String C_FUERATIEMPO = "FT";
    public static final String C_PENDIENTES = "PE";
    
    // Crea Actividad
    public static final String Q_INSERTA_ACTIVIDAD = CONFIG_PROPERTIES.getProperty("query.i_new_activitie");
    public static final String Q_GET_ID_IMPLEMENTACION = CONFIG_PROPERTIES.getProperty("query.c_id_implementacion");
    public static final String Q_SF_GET_STRUCTURA = CONFIG_PROPERTIES.getProperty("query.s_get_structura");
    public static final String Q_GET_STRUCT_FFM = CONFIG_PROPERTIES.getProperty("query._c_structura_ffm");
    
    //Edita actividad
    public static final String Q_UPDATE_ACTIVIDAD = CONFIG_PROPERTIES.getProperty("query.u_actualiza_actividad");
    
    //Elimina Actividad
    public static final String Q_DELETE_ACTIVIDAD = CONFIG_PROPERTIES.getProperty("query.d_elimina_actividad");
    
    
    // Cierra planeacion
    public static final String Q_GET_DATA_REFRESS_GANTT = CONFIG_PROPERTIES.getProperty("query.s_get_dataUpdate_gantt");
    public static final String Q_UPDATE_CIERRA_PLANEACION=CONFIG_PROPERTIES.getProperty("query.u_cierra_planeacion");
    
    
    // Termina plan
    public static final String Q_UPDATE_STATUS_PLAN=CONFIG_PROPERTIES.getProperty("query.u_termina_plan");
    public static final String Q_UPDATE_END_ACTIVIDADES=CONFIG_PROPERTIES.getProperty("query.u_termina_actividades");
    public static final String Q_GET_PROYECTOS_VIVOS=CONFIG_PROPERTIES.getProperty("query.get_proyectos_vivos");
    public static final String Q_GET_CSPS_FOR_COT = CONFIG_PROPERTIES.getProperty("query.s_get_csp_cot");
    public static final String Q_GET_CSPS_TERMINADOS_FFM = CONFIG_PROPERTIES.getProperty("query.get_csp_terminados_ffm");
    
    // Complementos 
    public static final String QR_INSERTA_COMENTARIOS = CONFIG_PROPERTIES.getProperty("query.inserta_comentarios");
    public static final String Q_UPDATE_COMENTARIO_GENERAL = CONFIG_PROPERTIES.getProperty("query.u_actualiza_comentariogeteral");
    
    public Constantes() {
        super();        
    }
    
    
    }
