package com.proyectactionsenl;

import com.proyectactionsenl.methods.CierraPlaneacion;
import com.proyectactionsenl.methods.ConsultaActividadForDocuments;
import com.proyectactionsenl.methods.ConsultaGeneralProyectosAsignados;
import com.proyectactionsenl.methods.ConsultaSitios;
import com.proyectactionsenl.methods.CreaActividad;
import com.proyectactionsenl.methods.EditaActividad;
import com.proyectactionsenl.methods.EliminaActividad;
import com.proyectactionsenl.methods.TerminaProyecto;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("ProyectActionsENL")
public class PMO_Actions {
    public PMO_Actions() {
        super();
    }
  
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("CreaActividad")
    public Response creaActividad(String InputCreaActividad){
       return new CreaActividad().creaActividad(InputCreaActividad);
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("EditaActividad")
    public Response editaActividad(String InputEditaActividad){
        return new EditaActividad().editaActividad(InputEditaActividad);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("EliminaActividad")
    public Response eliminaActividad(String InputEliminaActividad){
        return new EliminaActividad().eliminaActividad(InputEliminaActividad);
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("ConsultaSitiosSF")
    public Response consultaSitios(String InputConsultaSitios){
        return new ConsultaSitios().consultaSitios(InputConsultaSitios);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("FindProyectosSF")
    public Response consultaProyecto(String InputConsultaGeneralProyectosAsignados){
        return new ConsultaGeneralProyectosAsignados().consultaGeneralProyectosAsignados(InputConsultaGeneralProyectosAsignados);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("ConsultaActividadForDocument")
    public Response consultaActividadForDocuments(String InputConsultaActividadForDocuments){
        return new ConsultaActividadForDocuments().consultaActividadForDocuments(InputConsultaActividadForDocuments);   
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("TerminaProyecto")
    public Response terminaProyecto(String InputTerminaProyecto){
        return new TerminaProyecto().terminaProyecto(InputTerminaProyecto);
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("CierrePlaneacion")
    public Response cierraPlaneacion(String InputCierraPlaneacion){
        return new CierraPlaneacion().cierraPlaneacionProyecto(InputCierraPlaneacion);
    }    
}
