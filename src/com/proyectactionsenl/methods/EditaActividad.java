package com.proyectactionsenl.methods;

import com.proyectactionsenl.input.InputEditaActividad;
import com.proyectactionsenl.logica.Complementos;
import com.proyectactionsenl.logica.RecalculaGantt;
import com.proyectactionsenl.objects.Constantes;

import com.proyectactionsenl.output.OutputEditaActividad;

import javax.ws.rs.core.Response;

public class EditaActividad {
    public EditaActividad() {
        super();
    }
    
    
    public Response editaActividad(String InputEditaActividad){
        
        InputEditaActividad input_ws = (InputEditaActividad) Constantes.UTILS.strJsonToClass(InputEditaActividad, InputEditaActividad.class);
        OutputEditaActividad output_ws = new OutputEditaActividad();
        
        try{
            
            String dataImplementacion [][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_GET_ID_IMPLEMENTACION.replaceAll("idbridgesf", input_ws.getId_csp()));
            String idCuenta = dataImplementacion[0][1];
            
            Constantes.CRUD_FFM.actualizaDB(Constantes.Q_UPDATE_ACTIVIDAD.replaceAll("idac", input_ws.getId_tipo_actividad()).replaceAll("idres", input_ws.getId_responsable()).replaceAll("stadate", input_ws.getFecha_inicio()).replaceAll("enddate", input_ws.getFecha_fin()).replaceAll("staredate", input_ws.getFecha_inicio()).replaceAll("endredate", input_ws.getFecha_fin()).replaceAll("avpor", input_ws.getAvance()).replaceAll("iddepe", input_ws.getId_tarea_dependiente().equals("") ? "null" : input_ws.getId_tarea_dependiente()).replaceAll("iddact", input_ws.getId_actividad()));
            
            Complementos.registraComentarios(input_ws.getComentarios(), input_ws.getId_actividad(), input_ws.getId_user(),"");           
            
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            output_ws.setProyecto(new RecalculaGantt().recalculaGantt(idCuenta, input_ws.getId_csp()));
        
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error al editar actividad ex : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tr){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error al editar actividad tr : " + tr.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        //System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
    
}
