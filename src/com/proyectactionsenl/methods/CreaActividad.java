package com.proyectactionsenl.methods;

import com.proyectactionsenl.input.InputCreaActividad;

import com.proyectactionsenl.logica.Complementos;
import com.proyectactionsenl.logica.RecalculaGantt;
import com.proyectactionsenl.objects.Constantes;

import com.proyectactionsenl.output.OutputCreaActividad;

import javax.ws.rs.core.Response;

public class CreaActividad {
    public CreaActividad() {
        super();
    }
    
    
    public Response creaActividad(String InputCreaActividad){
        
        InputCreaActividad input_ws = (InputCreaActividad) Constantes.UTILS.strJsonToClass(InputCreaActividad, InputCreaActividad.class);
        OutputCreaActividad output_ws = new OutputCreaActividad();
        
        try{
            
            String dataImplementacion [][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_GET_ID_IMPLEMENTACION.replaceAll("idbridgesf", input_ws.getId_csp()));
            
            String idImplementacion = dataImplementacion[0][0];
            String idCuenta = dataImplementacion[0][1];
            
            String idActividad = Constantes.CRUD_FFM.insertaDBGetID(Constantes.Q_INSERTA_ACTIVIDAD.replaceAll("idact", input_ws.getId_tipo_actividad()).replaceAll("idresp", input_ws.getId_responsable()).replaceAll("idimp", idImplementacion).replaceAll("stadate", input_ws.getFecha_inicio()).replaceAll("enddate", input_ws.getFecha_fin()).replaceAll("staredate", input_ws.getFecha_inicio()).replaceAll("endredate", input_ws.getFecha_fin()).replaceAll("poav", input_ws.getAvance()).replaceAll("fath", input_ws.getId_tarea_dependiente().equals("") ? "null" : input_ws.getId_tarea_dependiente()),"PA_ID");
            
            Complementos.registraComentarios(input_ws.getComentarios(), idActividad, input_ws.getId_user(),"");           
                        
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            output_ws.setProyecto(new RecalculaGantt().recalculaGantt(idCuenta, input_ws.getId_csp()));
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error al crear actividad ex : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tr){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error al crear actividad tr : " + tr.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
    
}
