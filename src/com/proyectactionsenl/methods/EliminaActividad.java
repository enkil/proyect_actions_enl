package com.proyectactionsenl.methods;

import com.proyectactionsenl.input.InputEliminaActividad;

import com.proyectactionsenl.logica.Complementos;
import com.proyectactionsenl.logica.RecalculaGantt;
import com.proyectactionsenl.objects.Constantes;

import com.proyectactionsenl.output.OutputEliminaActividad;

import javax.ws.rs.core.Response;

public class EliminaActividad {
    public EliminaActividad() {
        super();
    }
    
    public Response eliminaActividad(String InputEliminaActividad){
        
        InputEliminaActividad input_ws = (InputEliminaActividad) Constantes.UTILS.strJsonToClass(InputEliminaActividad, InputEliminaActividad.class);
        OutputEliminaActividad output_ws = new OutputEliminaActividad();
        
        try{
            String dataImplementacion [][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_GET_ID_IMPLEMENTACION.replaceAll("idbridgesf", input_ws.getId_csp()));
            String idCuenta = dataImplementacion[0][1];
            
            Constantes.CRUD_FFM.eliminaDB(Constantes.Q_DELETE_ACTIVIDAD.replaceAll("idact", input_ws.getId_actividad()));
            
            Complementos.registraComentarios(input_ws.getComentarios(), input_ws.getId_actividad(), input_ws.getId_usuario(),"");
            
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            output_ws.setProyecto(new RecalculaGantt().recalculaGantt(idCuenta, input_ws.getId_csp()));
            
            }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error al crear actividad ex : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
            }catch(Throwable tr){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error al crear actividad tr : " + tr.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
            }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
            
    }
    
}
