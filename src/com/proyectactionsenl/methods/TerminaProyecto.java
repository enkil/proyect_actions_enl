package com.proyectactionsenl.methods;

import com.proyectactionsenl.input.InputTerminaProyecto;
import com.proyectactionsenl.logica.Complementos;
import com.proyectactionsenl.logica.RecalculaGantt;
import com.proyectactionsenl.objects.Constantes;
import com.proyectactionsenl.output.OutputTerminaProyecto;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.ArrayList;

import javax.ws.rs.core.Response;

public class TerminaProyecto {
    public TerminaProyecto() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response terminaProyecto(String InputTerminaProyecto){
        
        InputTerminaProyecto input_ws = (InputTerminaProyecto) Constantes.UTILS.strJsonToClass(InputTerminaProyecto, InputTerminaProyecto.class);
        OutputTerminaProyecto output_ws = new OutputTerminaProyecto();
        
        try{
            
           Boolean estatusEjecucuin = Constantes.CRUD_FFM.actualizaDB(Constantes.Q_UPDATE_STATUS_PLAN.replaceAll("idDetallePlaneacion", input_ws.getIdDetalleImplementacion()));
           
            if(estatusEjecucuin){
                Constantes.CRUD_FFM.actualizaDB(Constantes.Q_UPDATE_END_ACTIVIDADES.replaceAll("idDetallePlaneacion", input_ws.getIdDetalleImplementacion()).replaceAll("idUser", input_ws.getId_usuario()));
                
                String dataImplementacion [][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_GET_DATA_REFRESS_GANTT.replaceAll("idimplement", input_ws.getIdDetalleImplementacion()));
                String idBridgeSf = dataImplementacion[0][0];
                Complementos.registraComentarios(input_ws.getComentarios(), "null", input_ws.getId_usuario(),idBridgeSf);
                String idCuenta = dataImplementacion[0][1];
                
                this.terminaInplementacionCsp(idBridgeSf);
                
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion("Implementacion completada");
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setProyecto(new RecalculaGantt().recalculaGantt(idCuenta, idBridgeSf));
            }else{
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion("Error al cambiar estatus del plan");
                output_ws.setVersion(Constantes.WS_VERSION);
            }
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tx){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + tx.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
    
    private void terminaInplementacionCsp(String idCspSf){
        
        try{
            SObject[] csp = new SObject[1];
            SObject cspSf = new SObject();
            cspSf.setType(Constantes.CSP_TYPE);
            cspSf.setId(idCspSf);
            cspSf.setField("EstatusCSP__c", Constantes.STATUS_IMPLEMENTACION);
            csp[0] = cspSf;
            Constantes.CRUD_SF.update_sf(csp);
        }catch(Exception ex){
            System.err.println("Error al terminar implementacion proyecto : " + ex.toString());
        }    
    }
    
    
   /* private void terminaPlaneacion(String idCot,String idOportunidad){
        
        try{
            QueryResult datosCspCot = Constantes.CRUD_SF.query_sf(Constantes.Q_GET_CSPS_FOR_COT.replaceAll("idcot", idCot));
            ArrayList<String> arrBridgeSf = new ArrayList<String>();
            String arrCsps = "";
            
            if(datosCspCot.isDone()){
                if(datosCspCot.getSize()>0){
                    for(SObject elemneto : datosCspCot.getRecords()){
                        arrBridgeSf.add((String) elemneto.getField("Id"));        
                        if(Constantes.UTILS.validateDataSF(elemneto, "Cotizacion__r")){
                            SObject cot = (SObject) elemneto.getField("Cotizacion__r");
                            if(Constantes.UTILS.validateDataSF(cot, "Oportunidad__r")){
                                SObject opo = (SObject) cot.getField("Oportunidad__r");
                            }
                        }
                    }
                    
                    arrCsps = arrBridgeSf.toString().replace("[", "'").replace("]", "'").replaceAll(", ", "','"); 
                    
                    String proyectosTerminados = Constantes.CRUD_FFM.consultaBD(Constantes.Q_GET_CSPS_TERMINADOS_FFM.replaceAll("lista_csps", arrCsps))[0][0];
                    
                    if(Integer.valueOf(proyectosTerminados).equals(datosCspCot.getSize())){
                        SObject[] oportunidad = new SObject[1];
                        SObject op = new SObject();
                        op.setType(Constantes.OPPORTUNITY_TYPE);
                        op.setId(idOportunidad);
                        op.setField("EstatusImplementacion__c", Constantes.STATUS_IMPLEMENTACION);
                        oportunidad[0] = op;
                        Constantes.CRUD_SF.update_sf(oportunidad);
                    }
                }    
            }
            
        }catch(Exception ex){
            System.err.println("Error al terminar plan sf : " + ex.toString());
        }
        
    }*/
}
