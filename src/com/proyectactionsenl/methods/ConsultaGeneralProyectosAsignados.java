package com.proyectactionsenl.methods;

import com.proyectactionsenl.input.InputConsultaGeneralProyectosAsignados;
import com.proyectactionsenl.objects.Constantes;
import com.proyectactionsenl.objects.Cotizacion;
import com.proyectactionsenl.objects.Proyectos;
import com.proyectactionsenl.objects.Sitio;
import com.proyectactionsenl.output.OutputConsultaGeneralProyectosAsignados;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.Response;

public class ConsultaGeneralProyectosAsignados {
    public ConsultaGeneralProyectosAsignados() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response consultaGeneralProyectosAsignados(String InputConsultaGeneralProyectosAsignados){
        
        InputConsultaGeneralProyectosAsignados input_ws = (InputConsultaGeneralProyectosAsignados) Constantes.UTILS.strJsonToClass(InputConsultaGeneralProyectosAsignados, InputConsultaGeneralProyectosAsignados.class);
        OutputConsultaGeneralProyectosAsignados output_ws = new OutputConsultaGeneralProyectosAsignados();
        
        try{
            
            QueryResult proyectos_ass = Constantes.CRUD_SF.query_sf(Constantes.QR_GET_PROYECTOS_ASIGNADOS.replaceAll("id_pm", input_ws.getId_pm()));
            
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            output_ws.setProyectos(this.loadCotizacion(proyectos_ass));
            
        }catch(Exception ex){
            System.err.println("Error : " + ex.toString());
        }catch(Throwable tr){
            System.err.println("Error : " + tr.toString());    
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
    
    public Proyectos[] loadCotizacion(QueryResult proyectos_ass){
        Proyectos[] arr_proyectos = null;
        
        try{
            int cont = 0;
            Map<String,Proyectos> map_proyectos = new HashMap<String,Proyectos>();
            for(SObject res_obj : proyectos_ass.getRecords()){
                Proyectos proyectos = new Proyectos();
                if(res_obj.getSObjectField("Cot_Sitio__r")!=null && res_obj.getSObjectField("Cot_Sitio__r").getClass().equals(SObject.class)){
                    SObject cs = (SObject) res_obj.getSObjectField("Cot_Sitio__r");
                    if (cs != null) {
                        
                        if(cs.getSObjectField("Cotizacion__r")!=null && cs.getSObjectField("Cotizacion__r").getClass().equals(SObject.class)){
                            SObject cot = (SObject) cs.getSObjectField("Cotizacion__r");
                            if (cot != null) {
                                Cotizacion obj_cotizacion = new Cotizacion();
                                obj_cotizacion.setId_cotizacion((String) cot.getField("Id"));
                                obj_cotizacion.setFolio_cotizacion((String) cot.getField("Name"));
                                proyectos.setCotizaciones(obj_cotizacion);
                                                                                          
                                if(cot.getSObjectField("Oportunidad__r")!=null && cot.getSObjectField("Oportunidad__r").getClass().equals(SObject.class)){
                                    SObject oportunidad = (SObject) cot.getSObjectField("Oportunidad__r");
                                    if (oportunidad != null) {
                                        if(oportunidad.getSObjectField("Account")!=null && oportunidad.getSObjectField("Account").getClass().equals(SObject.class)){
                                            SObject cuenta = (SObject) oportunidad.getSObjectField("Account");
                                            if (cuenta != null) {
                                                proyectos.setNombre_cliente((String) cuenta.getField("RazonSocial__c"));
                                            }
                                        }
                                    }
                                }              
                            }
                            if(!map_proyectos.containsKey((String) cot.getField("Id"))){
                                map_proyectos.put((String) cot.getField("Id"), proyectos);
                            }        
                        } 
                    }
                    
                }
            }
            
            arr_proyectos = this.cargaCsp_To_Cot(map_proyectos, proyectos_ass);
            
        }catch(Exception ex){
            System.out.println("Error : " + ex.toString());
            arr_proyectos = null;
        }
        return arr_proyectos; 
    }
    
    public Proyectos[] cargaCsp_To_Cot(Map<String,Proyectos> map_proyectos,QueryResult detalle_csp){
        
        Proyectos[] arr_proyectos = new Proyectos[map_proyectos.size()];
        
        try{
            int cont = 0;
            for (Map.Entry<String, Proyectos> pro : map_proyectos.entrySet()) {
                Proyectos proye = pro.getValue();    
                Cotizacion cot = proye.getCotizaciones();
                cot.setSitios(this.cargaSitios(detalle_csp, cot.getId_cotizacion()));
                arr_proyectos[cont++] = proye;
            }
                      
        }catch(Exception ex){
            System.err.println("Error : " + ex.toString());
            arr_proyectos = null;
        }
        
        return arr_proyectos;
            
    }
    
    
    public Sitio[] cargaSitios(QueryResult detalle_csp,String key_cot){
        
        Sitio[] arr_sitio = null;
        
        try{
            int lengarr = 0;
            for(SObject sit : detalle_csp.getRecords()){
                if(sit.getSObjectField("Cot_Sitio__r")!=null && sit.getSObjectField("Cot_Sitio__r").getClass().equals(SObject.class)){
                   SObject cs = (SObject) sit.getSObjectField("Cot_Sitio__r");
                    if (cs != null) {
                        if(cs.getSObjectField("Cotizacion__r")!=null && cs.getSObjectField("Cotizacion__r").getClass().equals(SObject.class)){
                            SObject cot = (SObject) cs.getSObjectField("Cotizacion__r");
                            if (cot != null) {
                                if(((String) cot.getField("Id")).equals(key_cot)){
                                    lengarr++;        
                                }
                            }
                        }
                    }
                }
            }
            
            arr_sitio = new Sitio[lengarr];
            int contador = 0;
            for(SObject sit : detalle_csp.getRecords()){
                if(sit.getSObjectField("Cot_Sitio__r")!=null && sit.getSObjectField("Cot_Sitio__r").getClass().equals(SObject.class)){
                   SObject cs = (SObject) sit.getSObjectField("Cot_Sitio__r");
                    if (cs != null) {
                        if(cs.getSObjectField("Cotizacion__r")!=null && cs.getSObjectField("Cotizacion__r").getClass().equals(SObject.class)){
                            SObject cot = (SObject) cs.getSObjectField("Cotizacion__r");
                            if (cot != null) {
                                if(((String) cot.getField("Id")).equals(key_cot)){
                                    Sitio sitio_bd = new Sitio();
                                    sitio_bd.setId_csp((String) sit.getField("Id")); 
                                    sitio_bd.setName_csp((String) sit.getField("Name"));
                                    sitio_bd.setPaquete((String) sit.getField("NombrePlan__c"));
                                    //Cot_Sitio__c
                                    sitio_bd.setNum_cuenta_factura((String) sit.getField("CuentaFacturaNumero__c"));
                                    sitio_bd.setId_cuenta_factura((String) sit.getField("CuentaFactura__c")); 
                                    
                                    if(sit.getSObjectField("Cot_Sitio__r")!=null && sit.getSObjectField("Cot_Sitio__r").getClass().equals(SObject.class)){
                                        SObject cot_sitio = (SObject) sit.getSObjectField("Cot_Sitio__r");
                                        if (cot_sitio != null) {
                                                sitio_bd.setId_cs((String) cot_sitio.getField("Id"));
                                                sitio_bd.setNombre_cot_sitio((String) cot_sitio.getField("Name"));
                                                sitio_bd.setPlaza((String) cot_sitio.getField("Plaza__c"));
                                                sitio_bd.setDireccion_sitio((String) cot_sitio.getField("DireccionSitio__c"));
                                            if(cot_sitio.getSObjectField("Sitio__r")!=null && cot_sitio.getSObjectField("Sitio__r").getClass().equals(SObject.class)){
                                                SObject sitio = (SObject) cot_sitio.getSObjectField("Sitio__r");
                                                if (sitio != null) {
                                                    sitio_bd.setId_sitio((String) sitio.getField("Id"));
                                                    sitio_bd.setCalle((String) sitio.getField("Calle__c"));
                                                    sitio_bd.setColonia((String) sitio.getField("Colonia__c"));
                                                    sitio_bd.setEstado((String) sitio.getField("Estado__c"));
                                                    sitio_bd.setMunicipio((String) sitio.getField("DelegacionMunicipio__c"));
                                                    sitio_bd.setNum_interior((String) sitio.getField("NumeroInterior__c"));
                                                    sitio_bd.setNum_exterior((String) sitio.getField("NumeroExterior__c"));
                                                    //Name
                                                    sitio_bd.setNombre_cot_sitio((String) sitio.getField("IdSitio__c"));
                                                    sitio_bd.setLatitud((String) sitio.getField("Geolocalizacion__Latitude__s"));
                                                    sitio_bd.setLongitud((String) sitio.getField("Geolocalizacion__Longitude__s"));
                                                    
                                                    if(sitio.getSObjectField("ContactoPrincipalSitio__r")!=null && sitio.getSObjectField("ContactoPrincipalSitio__r").getClass().equals(SObject.class)){
                                                        SObject contacto = (SObject) sitio.getSObjectField("ContactoPrincipalSitio__r");
                                                        if (contacto != null) {
                                                            sitio_bd.setNombre_responsable_sitio((String) contacto.getField("Name"));
                                                            sitio_bd.setTelefono_contacto((String) contacto.getField("MobilePhone"));
                                                        }
                                                    }
                                                }
                                            }
                                            if(cot_sitio.getSObjectField("Owner")!=null && cot_sitio.getSObjectField("Owner").getClass().equals(SObject.class)){
                                                SObject propietario = (SObject) cot_sitio.getSObjectField("Owner");
                                                if (propietario != null) {
                                                    //sitio_bd.setNombre_responsable_sitio((String) propietario.getField("Name"));
                                                }
                                            }
                                        }
                                    }
                                    if(sit.getSObjectField("CuentaFactura__r")!=null && sit.getSObjectField("CuentaFactura__r").getClass().equals(SObject.class)){
                                        SObject cuentafactura = (SObject) sit.getSObjectField("CuentaFactura__r");
                                        if (cuentafactura != null) {
                                            sitio_bd.setRegion((String) cuentafactura.getField("RegionInstalacion__c"));       
                                            sitio_bd.setPlaza((String) cuentafactura.getField("Plaza__c"));
                                            sitio_bd.setDistrito((String) cuentafactura.getField("DistritoInstalacion__c"));
                                            sitio_bd.setCluster((String) cuentafactura.getField("ClusterInstalacion__c"));
                                            sitio_bd.setId_cuenta((String) cuentafactura.getField("IdCuenta__c"));
                                            sitio_bd.setCp((String) cuentafactura.getField("CodigoPostalInstalacion__c"));
                                        }
                                    }
                                    
                                    arr_sitio[contador++] = sitio_bd; 
                                }
                            }
                        }
                    }
                }
            }
            
        }catch(Exception ex){
            System.out.println("Error ex : " + ex.toString());
        }        
        
        return arr_sitio;
    }
}
