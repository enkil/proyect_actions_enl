package com.proyectactionsenl.methods;

import com.proyectactionsenl.input.InputCierraPlaneacion;
import com.proyectactionsenl.logica.Complementos;
import com.proyectactionsenl.logica.RecalculaGantt;
import com.proyectactionsenl.objects.Constantes;

import com.proyectactionsenl.output.OutputCierraPlaneacion;

import javax.ws.rs.core.Response;

public class CierraPlaneacion {
    public CierraPlaneacion() {
        super();
    }
    
    public Response cierraPlaneacionProyecto(String InputCierraPlaneacion){
        
        InputCierraPlaneacion input_ws = (InputCierraPlaneacion) Constantes.UTILS.strJsonToClass(InputCierraPlaneacion, InputCierraPlaneacion.class);
        OutputCierraPlaneacion output_ws = new OutputCierraPlaneacion();
        
        try{
            
            Boolean estatusEjecucion = Constantes.CRUD_FFM.actualizaDB(Constantes.Q_UPDATE_CIERRA_PLANEACION.replaceAll("idDetallePlaneacion", input_ws.getIdDetalleImplementacion()));
            
            if(estatusEjecucion){
                String dataImplementacion [][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_GET_DATA_REFRESS_GANTT.replaceAll("idimplement", input_ws.getIdDetalleImplementacion()));
                String idBridgeSf = dataImplementacion[0][0];
                String idCuenta = dataImplementacion[0][1];
                
                Complementos.registraComentarios(input_ws.getComentarios(), "null", input_ws.getId_usuario(),idBridgeSf);
                
                output_ws.setResult(Constantes.RESULT_SUSSES); 
                output_ws.setResultDescripcion("Planeacion cerrada"); 
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setProyecto(new RecalculaGantt().recalculaGantt(idCuenta, idBridgeSf));
            }else{
                output_ws.setResult(Constantes.RESULT_ERROR); 
                output_ws.setResultDescripcion("No se pudo actualizar el estatus de la implementacion"); 
                output_ws.setVersion(Constantes.WS_VERSION);
            }
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR); 
            output_ws.setResultDescripcion("ERROR CONSULTA SF : " + ex.toString()); 
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable ex){
            output_ws.setResult(Constantes.RESULT_ERROR); 
            output_ws.setResultDescripcion("ERROR CONSULTA SF : " + ex.toString()); 
            output_ws.setVersion(Constantes.WS_VERSION);
        } 
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
}
