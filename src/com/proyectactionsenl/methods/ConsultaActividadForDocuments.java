package com.proyectactionsenl.methods;

import com.proyectactionsenl.input.InputConsultaActividadForDocuments;
import com.proyectactionsenl.objects.Actividad;
import com.proyectactionsenl.objects.Constantes;
import com.proyectactionsenl.output.OutputConsultaActividadForDocuments;

import javax.ws.rs.core.Response;

public class ConsultaActividadForDocuments {
    public ConsultaActividadForDocuments() {
        super();
    }
    
    public Response consultaActividadForDocuments(String InputConsultaActividadForDocuments){
        
        InputConsultaActividadForDocuments input_ws = (InputConsultaActividadForDocuments) Constantes.UTILS.strJsonToClass(InputConsultaActividadForDocuments, InputConsultaActividadForDocuments.class);
        OutputConsultaActividadForDocuments output_ws = new OutputConsultaActividadForDocuments();
        
        try{
            
            String[][] act_for_doc = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_ACT_FOR_DOCUMENTS.replaceAll("id_css", input_ws.getId_cs()));
            if(act_for_doc.length > 0){
                Actividad[] arr_actividades = new Actividad[act_for_doc.length + 1];
                Actividad actividad_cs = new Actividad();
                
                actividad_cs.setId_actividad(input_ws.getId_cs());
                actividad_cs.setNombre_actividad("Documentacion de la punta");
                actividad_cs.setFecha_fin_real("");
                actividad_cs.setTipo_archivo("P");
                arr_actividades[0] = actividad_cs;
                for(int cont = 0; cont < act_for_doc.length; cont++){
                    Actividad actividad = new Actividad();
                    actividad.setId_actividad(act_for_doc[cont][0]);
                    actividad.setNombre_actividad(act_for_doc[cont][1]);
                    actividad.setFecha_fin_real(act_for_doc[cont][2]);
                    actividad.setTipo_archivo("A");
                    arr_actividades[cont + 1]=actividad;
                }
                
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setTotal_actividades(String.valueOf(act_for_doc.length + 1));
                output_ws.setActividades(arr_actividades);
                
            }else{
                
                Actividad[] arr_actividades = new Actividad[1];
                Actividad actividad_cs = new Actividad();
                
                actividad_cs.setId_actividad(input_ws.getId_cs());
                actividad_cs.setNombre_actividad("Documentacion de la punta");
                actividad_cs.setFecha_fin_real("");
                actividad_cs.setTipo_archivo("P");
                arr_actividades[0] = actividad_cs; 
                
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setTotal_actividades("1");
                output_ws.setActividades(arr_actividades);
            }
        
        }catch(Exception ex){
            System.err.println("Errr : " + ex.toString());
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable ed){
            System.err.println("Errr : " + ed.toString());
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
}
