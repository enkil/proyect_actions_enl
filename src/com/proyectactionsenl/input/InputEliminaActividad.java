package com.proyectactionsenl.input;

import com.proyectactionsenl.objects.Login;
import com.proyectactionsenl.utilerias.Utilerias;

public class InputEliminaActividad {
    
    private Login Login;
    private String Id_actividad;
    private String Id_usuario;
    private String Id_csp;
    private String Comentarios;
    
    public InputEliminaActividad() {
        super();
    }


    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_actividad(String Id_actividad) {
        this.Id_actividad = Utilerias.sanitizaParametros(Id_actividad);
    }

    public String getId_actividad() {
        return Id_actividad;
    }

    public void setId_usuario(String Id_usuario) {
        this.Id_usuario = Utilerias.sanitizaParametros(Id_usuario);
    }

    public String getId_usuario() {
        return Id_usuario;
    }

    public void setId_csp(String Id_csp) {
        this.Id_csp = Utilerias.sanitizaParametros(Id_csp);
    }

    public String getId_csp() {
        return Id_csp;
    }

    public void setComentarios(String Comentarios) {
        this.Comentarios = Utilerias.sanitizaParametros(Comentarios);
    }

    public String getComentarios() {
        return Comentarios;
    }
}
