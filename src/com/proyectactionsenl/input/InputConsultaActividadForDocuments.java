package com.proyectactionsenl.input;

import com.proyectactionsenl.objects.Login;
import com.proyectactionsenl.utilerias.Utilerias;

public class InputConsultaActividadForDocuments {
    
    private Login Login; 
    private String Id_cs;
    
    public InputConsultaActividadForDocuments() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_cs(String Id_cs) {
        this.Id_cs = Utilerias.sanitizaParametros(Id_cs);
    }

    public String getId_cs() {
        return Id_cs;
    }
}
