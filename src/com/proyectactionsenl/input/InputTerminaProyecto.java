package com.proyectactionsenl.input;

import com.proyectactionsenl.objects.Login;
import com.proyectactionsenl.utilerias.Utilerias;

public class InputTerminaProyecto {
    
    private Login Login; 
    private String idDetalleImplementacion;
    private String idOportunidadSf;
    private String idCotizacion;
    private String Id_usuario;
    private String Comentarios;
        
    public InputTerminaProyecto() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setIdDetalleImplementacion(String idDetalleImplementacion) {
        this.idDetalleImplementacion = Utilerias.sanitizaParametros(idDetalleImplementacion);
    }

    public String getIdDetalleImplementacion() {
        return idDetalleImplementacion;
    }
    
    public void setIdOportunidadSf(String idOportunidadSf) {
        this.idOportunidadSf = Utilerias.sanitizaParametros(idOportunidadSf);
    }

    public String getIdOportunidadSf() {
        return idOportunidadSf;
    }

    public void setId_usuario(String Id_usuario) {
        this.Id_usuario = Utilerias.sanitizaParametros(Id_usuario);
    }

    public String getId_usuario() {
        return Id_usuario;
    }

    public void setComentarios(String Comentarios) {
        this.Comentarios = Utilerias.sanitizaParametros(Comentarios);
    }

    public String getComentarios() {
        return Comentarios;
    }

    public void setIdCotizacion(String idCotizacion) {
        this.idCotizacion = Utilerias.sanitizaParametros(idCotizacion);
    }

    public String getIdCotizacion() {
        return idCotizacion;
    }

}
