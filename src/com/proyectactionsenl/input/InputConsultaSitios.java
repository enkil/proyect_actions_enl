package com.proyectactionsenl.input;

import com.proyectactionsenl.objects.Login;
import com.proyectactionsenl.utilerias.Utilerias;

public class InputConsultaSitios {
    
    private Login Login;
    private String Id_pm;
    
    public InputConsultaSitios() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_pm(String Id_pm) {
        this.Id_pm = Utilerias.sanitizaParametros(Id_pm);
    }

    public String getId_pm() {
        return Id_pm;
    }
}
