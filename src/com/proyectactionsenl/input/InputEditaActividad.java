package com.proyectactionsenl.input;

import com.proyectactionsenl.objects.Login;
import com.proyectactionsenl.utilerias.Utilerias;

public class InputEditaActividad {
    
    private Login Login;
    private String Id_tipo_actividad;
    private String Id_csp;
    private String Fecha_inicio;
    private String Fecha_fin;
    private String Id_responsable;
    private String Avance;
    private String Id_tarea_dependiente;
    private String Id_usuario;
    private String Comentarios;
    private String Id_actividad;  
    private String Id_user;
    
    
    public InputEditaActividad() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_csp(String Id_csp) {
        this.Id_csp = Utilerias.sanitizaParametros(Id_csp);
    }

    public String getId_csp() {
        return Id_csp;
    }

    public void setId_actividad(String Id_actividad) {
        this.Id_actividad = Utilerias.sanitizaParametros(Id_actividad);
    }

    public String getId_actividad() {
        return Id_actividad;
    }

    public void setFecha_inicio(String Fecha_inicio) {
        this.Fecha_inicio = Utilerias.sanitizaParametros(Fecha_inicio);
    }

    public String getFecha_inicio() {
        return Fecha_inicio;
    }

    public void setFecha_fin(String Fecha_fin) {
        this.Fecha_fin = Utilerias.sanitizaParametros(Fecha_fin);
    }

    public String getFecha_fin() {
        return Fecha_fin;
    }

    public void setId_responsable(String Id_responsable) {
        this.Id_responsable = Utilerias.sanitizaParametros(Id_responsable);
    }

    public String getId_responsable() {
        return Id_responsable;
    }

    public void setAvance(String Avance) {
        this.Avance = Avance;
    }

    public String getAvance() {
        return Avance;
    }

    public void setId_tarea_dependiente(String Id_tarea_dependiente) {
        this.Id_tarea_dependiente = Id_tarea_dependiente;
    }

    public String getId_tarea_dependiente() {
        return Id_tarea_dependiente;
    }

    public void setId_usuario(String Id_usuario) {
        this.Id_usuario = Id_usuario;
    }

    public String getId_usuario() {
        return Id_usuario;
    }

    public void setComentarios(String Comentarios) {
        this.Comentarios = Comentarios;
    }

    public String getComentarios() {
        return Comentarios;
    }

    public void setId_tipo_actividad(String Id_tipo_actividad) {
        this.Id_tipo_actividad = Id_tipo_actividad;
    }

    public String getId_tipo_actividad() {
        return Id_tipo_actividad;
    }

    public void setId_user(String Id_user) {
        this.Id_user = Id_user;
    }

    public String getId_user() {
        return Id_user;
    }
}
