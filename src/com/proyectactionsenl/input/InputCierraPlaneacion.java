package com.proyectactionsenl.input;

import com.proyectactionsenl.objects.Login;
import com.proyectactionsenl.utilerias.Utilerias;

public class InputCierraPlaneacion {
    
    private Login Login;
    private String idDetalleImplementacion;
    private String Id_usuario;
    private String Comentarios;
    
    public InputCierraPlaneacion() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setIdDetalleImplementacion(String idDetalleImplementacion) {
        this.idDetalleImplementacion = Utilerias.sanitizaParametros(idDetalleImplementacion);
    }

    public String getIdDetalleImplementacion() {
        return idDetalleImplementacion;
    }

    public void setId_usuario(String Id_usuario) {
        this.Id_usuario = Utilerias.sanitizaParametros(Id_usuario);
    }

    public String getId_usuario() {
        return Id_usuario;
    }

    public void setComentarios(String Comentarios) {
        this.Comentarios = Utilerias.sanitizaParametros(Comentarios);
    }

    public String getComentarios() {
        return Comentarios;
    }

}
