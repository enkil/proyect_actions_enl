package com.proyectactionsenl.utilerias;

import com.proyectactionsenl.objects.Constantes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;
import java.util.Map;

public class CrudFFM {
    private Connection con;
    private PreparedStatement pst;
    private ResultSet res;
    
    public CrudFFM() {
        super();
    }
    
    public String[][] consultaBD(String consulta){
        
        
        try {
            con = Constantes.CONECTION_DB.creaConexion(Constantes.DB_TYPE_CONECTION);
            pst = con.prepareStatement(consulta);
            res = pst.executeQuery();
            
            int columnas = res.getMetaData().getColumnCount();
            int filas = 0;
            
            while (res.next()){
                filas++;    
            }
            
            res = pst.executeQuery();
            
            String[][] matriz = new String[filas][columnas];
            int registro = 0;
            while (res.next()){
                
                for(int valor = 0; valor < columnas; valor++){
                    matriz[registro][valor] = res.getString(valor + 1);
                }
            registro++;
            }
            
            res.close();
            pst.close();
            
            return matriz;
        
        } catch(Exception ex) {
            System.out.println("Error al consultar bd : " + ex.toString());
            return null;
        } finally {
            if (con != null) {
                Constantes.CONECTION_DB.cerrarConexion(con);
            }
        }
    }
    
    public Boolean actualizaDB(String consulta) {
        
        //System.out.println("Actualizacion : "  + consulta);
        
        try {
            con = Constantes.CONECTION_DB.creaConexion(Constantes.DB_TYPE_CONECTION);
            pst = con.prepareStatement(consulta);
            int res = pst.executeUpdate();
            
            pst.close();
            return true;
            
        } catch(Exception ex) {
            System.out.println("Error al actualizar datos = " + ex.toString());
            return false;
        } finally {
            if (con != null) {
                Constantes.CONECTION_DB.cerrarConexion(con);
            }
        }
    }
    
    public Boolean eliminaDB(String consulta) {
        
        try {
            con = Constantes.CONECTION_DB.creaConexion(Constantes.DB_TYPE_CONECTION);
            pst = con.prepareStatement(consulta);
            int res = pst.executeUpdate();
            
            pst.close();
            return true;
            
        } catch(Exception ex) {
            System.out.println("Error al actualizar datos = " + ex.toString());
            return false;
        } finally {
            if (con != null) {
                Constantes.CONECTION_DB.cerrarConexion(con);
            }
        }
    }
    
    public Boolean insertaDB(String consulta) {
        
        try {
            
            //System.out.println("Insert : " + consulta);
            
            con = Constantes.CONECTION_DB.creaConexion(Constantes.DB_TYPE_CONECTION);
            pst = con.prepareStatement(consulta);
            int res = pst.executeUpdate();
            
            pst.close();
            return true;
            
        } catch(Exception ex) {
            System.out.println("Error al insertar datos = " + ex.toString());
            return false;
        } finally {
            if (con != null) {
                Constantes.CONECTION_DB.cerrarConexion(con);
            }
        }
    }
    
    public String insertaDBGetID(String consulta,String colum_index) {
        
        String llave_primaria = "-1";
        Connection con = null;
        PreparedStatement pst;
        ResultSet res;
        
        System.out.println("ID _ " + consulta + " colum : " + colum_index);
        
        try {
            con = Constantes.CONECTION_DB.creaConexion(Constantes.DB_TYPE_CONECTION);
            pst = con.prepareStatement(consulta,new String[]{colum_index});
            pst.executeUpdate();
            res = pst.getGeneratedKeys();
            
            while (res.next()) {
                llave_primaria = res.getString(1);
            }
            
            pst.close();
            res.close();
            
            return llave_primaria;            
        } catch(Exception ex) {
            System.out.println("Error al insertar datos = " + ex.toString());
            return "ERRO INSERT -1 : " + ex.toString();
        } finally {
            if (con != null) {
                Constantes.CONECTION_DB.cerrarConexion(con);
            }
        }
    }
    
    public String getSessionId_FFM() {

        String sesion_id_sf = "-1";
        
        try {
            
            String consulta = Constantes.QR_GET_ID_SESSION_SF;
            con = Constantes.CONECTION_DB.creaConexion(Constantes.DB_TYPE_CONECTION);
            pst = con.prepareStatement(consulta);
            res = pst.executeQuery();

            while (res.next()) {
                sesion_id_sf = (String) res.getString(1);
            }

            res.close();
            pst.close();
    
        } catch(Exception ex) {
            System.out.println("Error al consultar id de sesion : " + ex.toString());
            sesion_id_sf = "-1";
        } finally {
            if (con != null) {
                Constantes.CONECTION_DB.cerrarConexion(con);
            }
        }
        
        return sesion_id_sf;

    }
    
    public Map <String,String> configuracionGeneral(){

        Map<String,String> configuraciones = new HashMap<String,String>();

        try{

            con = Constantes.CONECTION_DB.creaConexion(Constantes.DB_TYPE_CONECTION);
            pst = con.prepareStatement(Constantes.QR_CONFIG_IN_DB);
            res = pst.executeQuery();

            while (res.next()) {
                configuraciones.put(res.getString(2), res.getString(3));
            }
            
            res.close();
            pst.close();

        }catch(Exception ex) {
            System.err.println("Error " + ex.toString());
        }finally{
            if(con != null){
                Constantes.CONECTION_DB.cerrarConexion(con);
            }
        }

    return configuraciones;

    }
    
    
    }