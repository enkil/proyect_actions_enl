package com.proyectactionsenl.utilerias;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import com.proyectactionsenl.objects.Constantes;
import com.proyectactionsenl.objects.Variables;

import com.sforce.soap.partner.sobject.SObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.owasp.html.PolicyFactory;
import static org.owasp.html.Sanitizers.BLOCKS;
import static org.owasp.html.Sanitizers.FORMATTING;
import static org.owasp.html.Sanitizers.IMAGES;
import static org.owasp.html.Sanitizers.LINKS;
import static org.owasp.html.Sanitizers.STYLES;
import static org.owasp.html.Sanitizers.TABLES;

public class Utilerias {
    public Utilerias() {
        super();
    }
    
    private static PolicyFactory sanitiser = BLOCKS.and(FORMATTING).and(IMAGES).and(LINKS).and(STYLES).and(TABLES);
    
    
    public static String sanitizaParametros(String parametros){
        return sanitiser.sanitize(parametros);    
    }
    
    public Object strJsonToClass(String jsonInput, Class<?> claseConversion){
        
        try{
        
            Gson gson_object = new Gson();
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(jsonInput);
            
            return gson_object.fromJson(element, claseConversion);
        
        }catch(Exception ex){
            System.err.println("error : " + ex.toString());
            return null;
        }
                
    }
    
    public String classTostrJson(Object classToJson){
        try{
            Gson gson = new Gson();
            
            return gson.toJson(classToJson);
        }catch(Exception ex){
            System.out.println("Error json to class : " + ex);
            return "{}";
        }    
    }
    
    public Properties getConfiguracion(){
        Properties prop = null;
        try{
            prop = new Properties();
            prop.load(this.getClass().getResourceAsStream(Constantes.PATH_PROPERTIES));
            
            return prop;
        }catch(Exception ex){
            System.err.println("Error al obtener propiedades = " + ex);
            return prop;
        }
    }
    
    public Properties getConfiguracionBD(){
        
        Properties prop = new Properties();
        
        try{
            
             Map<String,String> configuraciones = Constantes.CRUD_FFM.configuracionGeneral();
              
              if(!configuraciones.isEmpty()){
                  for ( Map.Entry<String, String> entry : configuraciones.entrySet()) {
                      String key = entry.getKey();
                      String value = entry.getValue();
                      prop.put(key, value);  
                  }
              }
            
        }catch(Exception ex){
            System.out.println("Error propiedades BD " + ex.toString());
        }
        
        return prop;
    }
    
    
    public void loadConfigIN_DB(){
        
        try{
            
            Properties prop_in_db = this.getConfiguracionBD();
            Variables.setSF_US(prop_in_db.getProperty(Constantes.KEY_US_SF));
            Variables.setSF_PA(prop_in_db.getProperty(Constantes.KEY_PA_SF));
            Variables.setSF_END_POINT(prop_in_db.getProperty(Constantes.KEY_END_POINT_SF));
            Variables.setSEGUNDOS_DOS_DIAS(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_DOS_DIAS));
            Variables.setSEGUNDOS_MEDIODIA(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_MEDIODIA));
            Variables.setSEGUNDOS_UNDIA(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_UNDIA));
            Variables.setPORCENTAJE_OK(prop_in_db.getProperty(Constantes.KEY_PORCENTAJE_OK));
            Variables.setPORCENTAJE_RIESGO(prop_in_db.getProperty(Constantes.KEY_PORCENTAJE_RIESGO));
            Variables.setCOLOR_FUERA_TIEMPO(prop_in_db.getProperty(Constantes.KEY_COLOR_FUERA_TIEMPO));
            Variables.setCOLOR_RIESGO(prop_in_db.getProperty(Constantes.KEY_COLOR_RIESGO));
            Variables.setCOLOR_ENTIEMPO(prop_in_db.getProperty(Constantes.KEY_COLOR_ENTIEMPO));
            Variables.setKPI_PE(prop_in_db.getProperty(Constantes.KEY_KPI_PE_DIAS));
            Variables.setKPI_PI(prop_in_db.getProperty(Constantes.KEY_KPI_PI_DIAS));
            Variables.setQR_BANDEJA_MESA_CONTROL(prop_in_db.getProperty(Constantes.KEY_QR_PROYECTOS_MESA));
            Variables.setQR_BANDEJA_EN_PLANEACION(prop_in_db.getProperty(Constantes.KEY_QR_BANDEJA_EN_PLANEACION));
            Variables.setQR_BANDEJA_FACTURANDO(prop_in_db.getProperty(Constantes.KEY_QR_BANDEJA_FACTURANDO)); 
            Variables.setURL_DASHBOARD(prop_in_db.getProperty(Constantes.KEY_URL_DASHBOARD));
           
        }catch(Exception ex){
            System.err.println("Error al cargar configuracion en DB : " + ex.toString());
        }    
    }    
    
    public Calendar getDateSF(String fechaAgenda_sf){
        
        try{

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(fechaAgenda_sf);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);            
            return cal; 
            
        }catch(Exception ex){
            System.out.println("error fecha sf : " + ex.toString());
            return null;
        }
        
    }
    
    public String tiempoTranscurrido(String fecha_creacion){
        
        try{
            
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
     
            Date fechaFinal=new Date();
            Date fechaInicial=format.parse(fecha_creacion);
    
            int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
            int dias=0;
            int horas=0;
            int minutos=0;
            
            if(diferencia>86400) {
                dias=(int)Math.floor(diferencia/86400);
                diferencia=diferencia-(dias*86400);
            }
            if(diferencia>3600) {
                horas=(int)Math.floor(diferencia/3600);
                diferencia=diferencia-(horas*3600);
            }
            if(diferencia>60) {
                minutos=(int)Math.floor(diferencia/60);
                diferencia=diferencia-(minutos*60);
            }
            
            return dias+" dias, "+horas+" horas, "+minutos+" minutos";
           
        }catch(Exception ex){
            System.out.println("Error : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public String getQueryBandejas_SF(String tipo_bandeja, String id_propietario) {

        String key_consulta = "";

        if (id_propietario.equals("1") || id_propietario == "1") {
            switch (tipo_bandeja) {
            case "AG":
                    key_consulta = "QR_BA_AG_ENL";
                    break;
            case "AC":
                    key_consulta = "QR_BA_AC_ENL";
                    break;
            case "RV":
                    key_consulta = "QR_BA_RV_ENL";
                    break;
            }
        } else if (id_propietario.equals("16") || id_propietario == "16") {
            switch (tipo_bandeja) {
            case "AG":
                    key_consulta = "QR_BA_AG_RC";
                    break;
            case "AC":
                    key_consulta = "QR_BA_AC_RC";
                    break;
            case "RV":
                    key_consulta = "QR_BA_RV_RC";
                    break;
            }
        }
        
        return key_consulta;
    }
    
    public String getSemaforoXPlanear(String fecha_creacion){
        
        String semaforo = "";
        
        try{
            
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
     
            Date fechaFinal=new Date();
            Date fechaInicial=format.parse(fecha_creacion);
    
            int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
            
            
            if(diferencia <= Integer.valueOf(Variables.getSEGUNDOS_UNDIA())) {
                semaforo = Variables.getCOLOR_ENTIEMPO();
            }
            if(diferencia > Integer.valueOf(Variables.getSEGUNDOS_UNDIA()) && diferencia <= Integer.valueOf(Variables.getSEGUNDOS_MEDIODIA())) {
                semaforo = Variables.getCOLOR_RIESGO();
            }
            if(diferencia > Integer.valueOf(Variables.getSEGUNDOS_MEDIODIA())) {
                semaforo = Variables.getCOLOR_FUERA_TIEMPO();
            }
            
            return semaforo;
            
        }catch(Exception ex){
            System.out.println("Error : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public String getEnTiempo(String fechaFinPlan, String fechaFinReal){
        
        String enTiempo = ""; 
        
        try{
            
            if(fechaFinPlan!=null && fechaFinReal!=null && !fechaFinPlan.equals("") && !fechaFinReal.equals("")){
                Float diferenciaEn_ms = (Float.valueOf(Constantes.UTILS.StringToDateMM(fechaFinReal).getTime()) - Float.valueOf(Constantes.UTILS.StringToDateMM(fechaFinPlan).getTime())) / (1000 * 60 * 60 * 24);
                
                if(diferenciaEn_ms<0){
                    enTiempo = "true";
                }else{
                    enTiempo = "false";
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error en get tiempo : " + ex.toString());
        }    
        
        return enTiempo;
    }
    
    public String getSemaforoEnPlaneacion(String porcentaje_esperado,String porcentaje_real,String estatusPlaneacion, String enTiempo){
        
        String semaforo = "";
        
        try{
                                   
                Integer calculo;
                
                if(enTiempo.equals("true")){
                    calculo =  1;
                }else{
                    if(estatusPlaneacion.equals("0") || estatusPlaneacion.equals("")){
                        calculo =  2;    
                    }else{
                        calculo =  3;
                    }
                }
                
                //Plan en tiempo
                if(calculo.equals(1)){
                    if(Float.valueOf(porcentaje_real) - Float.valueOf(porcentaje_esperado) >= (0 + Float.valueOf(Variables.getPORCENTAJE_OK()))){
                        semaforo = Variables.getCOLOR_ENTIEMPO();
                    }else if(((Float.valueOf(porcentaje_real)- Float.valueOf(porcentaje_esperado)) < 0) && ((Float.valueOf(porcentaje_real)- Float.valueOf(porcentaje_esperado)) * -1) <= Float.valueOf(Variables.getPORCENTAJE_RIESGO())){
                        semaforo = Variables.getCOLOR_RIESGO();   
                    }else{
                        semaforo = Variables.getCOLOR_FUERA_TIEMPO();
                    } 
                //Plan fuera de tiempo pero con planeacion viva     
                }else if(calculo.equals(2)){
                    if(((Float.valueOf(porcentaje_real)- Float.valueOf(porcentaje_esperado)) < 0) && ((Float.valueOf(porcentaje_real)- Float.valueOf(porcentaje_esperado)) * -1) <= Float.valueOf(Variables.getPORCENTAJE_RIESGO())){
                        semaforo = Variables.getCOLOR_RIESGO();   
                    }else{
                        semaforo = Variables.getCOLOR_FUERA_TIEMPO();
                    } 
                //-- Fuera de tiempo planeacion cerrada    
                }else{
                    semaforo = Variables.getCOLOR_FUERA_TIEMPO();    
                }          
                  
                return semaforo;
            
        }catch(Exception ex){
            System.out.println("Error semaforo : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public String getSemaforoMesa(String fecha_creacion){
        
        String semaforo = "";
        
        try{
            
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
     
            Date fechaFinal=new Date();
            Date fechaInicial=format.parse(fecha_creacion);
    
            int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
            
            
            if(diferencia <= Integer.valueOf(Variables.getSEGUNDOS_UNDIA())) {
                semaforo = Variables.getCOLOR_ENTIEMPO();
            }
            if(diferencia > Integer.valueOf(Variables.getSEGUNDOS_UNDIA()) && diferencia <= Integer.valueOf(Variables.getSEGUNDOS_MEDIODIA())) {
                semaforo = Variables.getCOLOR_RIESGO();
            }
            if(diferencia > Integer.valueOf(Variables.getSEGUNDOS_MEDIODIA())) {
                semaforo = Variables.getCOLOR_FUERA_TIEMPO();
            }
            
            return semaforo;
            
        }catch(Exception ex){
            System.out.println("Error : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public String toInitCap(String param) {
        String cadena_return = "";
        String cadena_base = "";
        if (param != null && param.length() > 0) {
            String[] palabras = param.split(" ");
            Boolean contiene_numero = false;
            
            for(String palabra : palabras){
                if(palabra.contains("@") || palabra.contains("-") || palabra.contains("$") || palabra.contains("CSP") || palabra.contains("COT") || palabra.contains("SIT") || palabra.contains("CS")){
                    cadena_base = palabra + " ";        
                }else{
                    char[] charArray = palabra.toLowerCase().toCharArray();
                    if(charArray.length > 0){
                        charArray[0] = Character.toUpperCase(charArray[0]);
                        for (int i = 0; i < palabra.length()- 2; i++){
                            if (charArray[i] == ' ' || charArray[i] == '.' || charArray[i] == ','){
                                charArray[i + 1] = Character.toUpperCase(charArray[i + 1]);    
                            }
                        }
                        
                        if(contiene_numero){
                            cadena_base = palabra + " ";
                        }else{
                            cadena_base = new String(charArray) + " ";
                        } 
                    }else{
                        cadena_base=""; 
                    }
                    
                }
                
                cadena_return = cadena_return + cadena_base;
                
            }   
            
            return cadena_return.trim();
        } else {
            return "";
        }
    }
    
    public Boolean validateDataSF(SObject contenedor, String campoValidar){
        
        try{
            if(contenedor.getSObjectField(campoValidar) != null && contenedor.getSObjectField(campoValidar).getClass().equals(SObject.class)){
                 SObject objeto_sf = (SObject) contenedor.getSObjectField(campoValidar); 
                 if (objeto_sf != null) {
                     return true;
                 }else{
                    return false;    
                 }
            }else{
                return false;    
            }
        }catch(Exception ex){
            return false;    
        }
    }
    
    public String getPorcentajeEsperado(String fechaIn, String fechaFin, String fechaActual){
        String porcentaje_esperado = "0.0";
        DecimalFormat df = new DecimalFormat("#.00");
        
        //System.out.println(fechaIn +" - " + fechaFin + " - " + fechaActual);
        
        try{
            
            if(!fechaIn.equals("") && !fechaFin.equals("")){
                Float diferenciaEn_ms = (Float.valueOf(Constantes.UTILS.StringToDateMM(fechaFin).getTime()) - Float.valueOf(Constantes.UTILS.StringToDateMM(fechaIn).getTime())) / (1000 * 60 * 60 * 24);
                Float diferencia_dos = (Float.valueOf(Constantes.UTILS.StringToDateMM(fechaActual).getTime()) - Float.valueOf(Constantes.UTILS.StringToDateMM(fechaIn).getTime()))  / (1000 * 60 * 60 * 24);
                Float esperado = 0.0f;
                
                if(diferenciaEn_ms==0){
                    esperado = (100) * diferencia_dos;
                }else{
                    esperado = (100/diferenciaEn_ms) * diferencia_dos;
                }
                
                //System.out.println(diferenciaEn_ms + "- "+ diferencia_dos + " - " + esperado);
                
                if(esperado != null){
                    porcentaje_esperado = (Float.valueOf(df.format(esperado)) >= 100 ? "100" : Float.valueOf(df.format(esperado)).toString());    
                }                
            }
            
        }catch(Exception ex){
            System.err.println("Error calcula fecha : " + ex.toString());
        }     
        
        return porcentaje_esperado;
    }
    
    public Date StringToDateMM(String fecha){
          Date date = null;
        try{
            //Instantiating the SimpleDateFormat class
            SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
            //Parsing the given String to Date object
            date = formatter.parse(fecha);
            //System.out.println("Date object value: "+date);
            
        }catch(Exception ex){
            System.err.println("Error strToDate : " + ex.toString());
        }
         return date;
      }
    
    public String minDate(ArrayList<Date> listDate){
        
        String fecha = "";
        
        try{
            
            Collections.sort(listDate);
            
            if(listDate.size() > 0){
                fecha = Constantes.UTILS.dateToString(listDate.get(0));
            }
            
            
        }catch(Exception ex){
            System.err.println("Error al calcular fechas minMax : " + ex.toString());    
        }
        
        return fecha;
    }
    
    public String maxDate(ArrayList<Date> listDate){
        
        String fecha = "";
        
        try{
            
            Collections.sort(listDate);
            
            if(listDate.size() > 0){
                fecha = Constantes.UTILS.dateToString(listDate.get(listDate.size() - 1));
            }
            
        }catch(Exception ex){
            System.err.println("Error al calcular fechas minMax : " + ex.toString());    
        }
        
        return fecha;
    }
    
    public String dateToString(Date fecha){
        
        String todayAsString = "";
        
        try{
            String pattern = "MM-dd-yyyy";

            DateFormat df = new SimpleDateFormat(pattern);
            
            todayAsString = df.format(fecha);


            //System.out.println("Today is: " + todayAsString);    
        }catch(Exception ex){
            System.err.println(ex.toString());    
        }
        
        return todayAsString;
    }
    
    public String valorDosDecimales(String valor){
        
        DecimalFormat df = new DecimalFormat("#.00");
        String valorFormateado = valor;
        try{
            
            valorFormateado = df.format(Float.valueOf(valor));
        
        }catch(Exception ex){
            System.err.println("Error al calcular valor dos decimales " + ex.toString());
        } 
        
        return valorFormateado;        
    }
    
    public Map<String,String> getContadoresPlaneacion(ArrayList<String> listSemaforo){
        
        Map<String,String> map_cont_imple = new HashMap<String,String>();
        
        Integer puntas_en_tiempo = 0;
        Integer puntas_en_riesgo = 0;
        Integer puntas_fuera_tiempo = 0;
        Integer pendienetes = 0;
        
        try{
            
            for(String semaforo : listSemaforo){
                if(semaforo.equals(Variables.getCOLOR_ENTIEMPO())){
                    ++puntas_en_tiempo;
                }else if(semaforo.equals(Variables.getCOLOR_RIESGO())){
                    ++puntas_en_riesgo;
                }else if(semaforo.equals(Variables.getCOLOR_FUERA_TIEMPO())){
                    ++puntas_fuera_tiempo;
                }else{
                    ++pendienetes;    
                }
            }
                
            map_cont_imple.put(Constantes.C_ENTIEMPO, puntas_en_tiempo.toString());
            map_cont_imple.put(Constantes.C_ENRIESGO, puntas_en_riesgo.toString());
            map_cont_imple.put(Constantes.C_FUERATIEMPO, puntas_fuera_tiempo.toString());
            map_cont_imple.put(Constantes.C_PENDIENTES, pendienetes.toString());
            
        }catch(Exception ex){
            System.err.println("Error al cargar contadores");
        } 
        
        return map_cont_imple;
    }
}
